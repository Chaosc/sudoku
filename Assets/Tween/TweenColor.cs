﻿using UnityEngine;
using System.Collections;

public class TweenColor : Tween {

    public Color color = Color.white;
    public bool includeChildren = true;

    public override void PlayNow() {
        iTween.ColorTo(gameObject, iTween.Hash("color", color,
            "time", time, "delay", delay, "easetype", easeType, "looptype", loopType,
            "includechildren", includeChildren,
            "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this));
    }

    public override string Type() {
        return "color";
    }
}
