﻿using UnityEngine.UI;

public class TweenFillAmount : TweenFloat {

    private Image image;

    void Awake() {
        image = GetComponent<Image>();
    }

    protected override void OnValueUpdate(float value) {
        base.OnValueUpdate(value);
        image.fillAmount = value;
    }
}
