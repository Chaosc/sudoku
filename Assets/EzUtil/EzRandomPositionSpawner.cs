﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzRandomPositionSpawner<T, P> : EzRandomSpawner<T, P> where T : Component where P : Component {

    public Transform positions;

    private List<Vector3> spawnPositions;

    private Vector3 pickPosition {
        get {
            int index = Random.Range(0, (positions.childCount - maxCount) / 2);
            Vector3 position = spawnPositions[index];
            spawnPositions.RemoveAt(index);
            return position;
        }
    }

    protected override void Start() {
        if (positions == null) {
            positions = transform.Find("Positions");
        }
        if (maxCount <= 0 || maxCount > positions.childCount - 2) {
            Debug.LogWarning("maxCount must between 0 and positions.childCount - 2");
            return;
        }
        spawnPositions = new List<Vector3>();
        for (int i = 0; i < positions.childCount; ++i) {
            spawnPositions.Add(positions.GetChild(i).position);
        }
        base.Start();
    }

    protected override void OnSpawn(T obj) {
        obj.transform.position = pickPosition;
        base.OnSpawn(obj);
    }

    public override void Recycle(Transform child) {
        spawnPositions.Add(child.position);
        base.Recycle(child);
    }
}
