﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshSaver : MonoBehaviour {

    public Mesh FindMesh() {
        Mesh mesh = null;
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if (meshFilter != null) {
            mesh = meshFilter.sharedMesh;
        } else {
            SkinnedMeshRenderer skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
            if (skinnedMeshRenderer != null) {
                mesh = skinnedMeshRenderer.sharedMesh;
            }
        }
        return mesh;
    }
}
