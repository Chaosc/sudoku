﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MeshSaver))]
public class MeshSaverEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        MeshSaver meshSaver = target as MeshSaver;
        Mesh mesh = meshSaver.FindMesh();
        if (mesh == null) {
            Debug.LogError("Cannot find mesh on this GameObject");
            return;
        }
        if (GUILayout.Button("Save Mesh")) {
            string savePath = EditorUtility.SaveFilePanelInProject("Save mesh", "Mesh.asset", "asset",
                "Please enter a file name to save the mesh to");
            if (!string.IsNullOrEmpty(savePath)) {
                Debug.Log("Saved Mesh to:" + savePath);
                AssetDatabase.CreateAsset(mesh, savePath);
                AssetDatabase.Refresh();
            }
        }
    }
}
