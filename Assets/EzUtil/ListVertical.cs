﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListVertical : MonoBehaviour {

    public float itemHeight;

	// Use this for initialization
	void Start() {
		
	}
	
	// Update is called once per frame
	void Update() {
		for (int i = 0; i < transform.childCount; ++i) {
            transform.GetChild(i).localPosition = GetPositionAt(i);
        }
	}

    Vector3 GetPositionAt(int index) {
        return new Vector3(0, itemHeight * index, 0);
    }
}
