﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProperty<T> {

    string Name { get; }

    bool Valid { get; }

    T Get(object target);

    void Set(object target, T value);

}
