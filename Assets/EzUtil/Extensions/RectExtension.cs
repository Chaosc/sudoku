﻿using UnityEngine;
using System.Collections;

public static class RectExtension {

    public static Bounds ToBounds(this Rect self) {
        return new Bounds(self.center, self.size);
    }

    public static Rect NewX(this Rect self, float x) {
        return new Rect(x, self.y, self.width, self.height);
    }

    public static Rect NewY(this Rect self, float y) {
        return new Rect(self.x, y, self.width, self.height);
    }

    public static Rect NewXY(this Rect self, float x, float y) {
        return new Rect(x, y, self.width, self.height);
    }

    public static Rect NewW(this Rect self, float width) {
        return new Rect(self.x, self.y, width, self.height);
    }

    public static Rect NewH(this Rect self, float height) {
        return new Rect(self.x, self.y, self.width, height);
    }

    public static Rect NewWH(this Rect self, float width, float height) {
        return new Rect(self.x, self.y, width, height);
    }

    public static Rect NewCenter(this Rect self, Vector2 center) {
        return new Rect(center, self.size);
    }

    public static Rect NewSize(this Rect self, Vector2 size) {
        return new Rect(self.position, size);
    }

    public static Rect GetZoomed(this Rect self, float size) {
        return self.GetZoomed(new Vector2(size, size));
    }

    public static Rect GetZoomed(this Rect self, Vector2 cornerOffset) {
        return self.GetZoomed(cornerOffset, cornerOffset);
    }

    public static Rect GetZoomed(this Rect self, Vector2 minCornerOffset, Vector2 maxCornerOffset) {
        Vector2 min = self.min - minCornerOffset;
        Vector2 max = self.max + maxCornerOffset;
        Vector2 size = max - min;
        return new Rect(min, size);
    }

    public static Rect GetScaled(this Rect self, float scale) {
        return self.GetScaled(scale, scale);
    }

    public static Rect GetScaled(this Rect self, Vector2 scale) {
        return self.GetScaled(scale.x, scale.y);
    }

    public static Rect GetScaled(this Rect self, float scaleX, float scaleY) {
        float width = self.width * scaleX;
        float height = self.height * scaleY;
        float x = self.center.x - width * 0.5f;
        float y = self.center.y - height * 0.5f;
        return new Rect(x, y, width, height);
    }

    public static Rect GetMerged(this Rect source, Rect target) {
        if (source.size == Vector2.zero) {
            return target;
        }
        Vector2 min = new Vector2();
        Vector2 max = new Vector2();
        min.x = Mathf.Min(source.min.x, target.min.x);
        min.y = Mathf.Min(source.min.y, target.min.y);
        max.x = Mathf.Max(source.max.x, target.max.x);
        max.y = Mathf.Max(source.max.y, target.max.y);
        Vector2 size = max - min;
        return new Rect(min, size);
    }

    public static bool Contains(this Rect source, Rect target) {
        return source.Contains(target.min) && source.Contains(target.max);
    }

    public static bool Intersects(this Rect source, Rect target) {
        return (source.Contains(target.min) && !source.Contains(target.max))
            || (source.Contains(target.max) && !source.Contains(target.min));
    }

    public static float GetPerimeter(this Rect self) {
        return self.width * 2 + self.height * 2;
    }

    public static Vector2 RandomPosition(this Rect self) {
        return new Vector2(Random.Range(self.xMin, self.xMax), Random.Range(self.yMin, self.yMax));
    }

    public static Vector2 RandomPositionOnEdge(this Rect self) {
        float[] sections = new float[] { self.width, self.height, self.width, self.height };
        float result = 0;
        int edge = EzRandom.GetRoll(sections, out result);
        switch (edge) {
            case 0:
                return self.min.OffsetX(result);
            case 1:
                return self.max.OffsetY(-(result - self.width));
            case 2:
                return self.max.OffsetX(-(result - self.width - self.height));
            case 3:
                return self.min.OffsetY(result - self.width * 2 - self.height);
        }
        throw new System.Exception("unexpected result");
    }
}
