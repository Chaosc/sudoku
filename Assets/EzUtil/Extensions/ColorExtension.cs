﻿using UnityEngine;
using System.Collections;

public static class ColorExtension {

	public static Color NewR(this Color color, float r) {
        return new Color(r, color.g, color.b, color.a);
    }

    public static Color NewG(this Color color, float g) {
        return new Color(color.r, g, color.b, color.a);
    }

    public static Color NewB(this Color color, float b) {
        return new Color(color.r, color.g, b, color.a);
    }

    public static Color NewA(this Color color, float a) {
        return new Color(color.r, color.g, color.b, a);
    }

    public static HSVColor ToHSV(this Color color) {
        return new HSVColor(color);
    }

    public static Color NewH(this Color color, int h) {
        HSVColor hsvColor = color.ToHSV();
        hsvColor.h = h;
        return hsvColor.rgb;
    }

    public static Color NewS(this Color color, int s) {
        HSVColor hsvColor = color.ToHSV();
        hsvColor.s = s;
        return hsvColor.rgb;
    }

    public static Color NewV(this Color color, int v) {
        HSVColor hsvColor = color.ToHSV();
        hsvColor.v = v;
        return hsvColor.rgb;
    }

    public static Color Fade(this Color from, float a, float t) {
        return NewA(from, Mathf.Lerp(from.a, a, t));
    }

    public static Color Tint(this Color from, Color to, float t) {
        return new Color(
            Mathf.Lerp(from.r, to.r, t),
            Mathf.Lerp(from.g, to.g, t),
            Mathf.Lerp(from.b, to.b, t),
            Mathf.Lerp(from.a, to.a, t));
    }
}
