﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzLevelExp {

    private const float levelExpPower = 1.6f;
    private const float levelExpMultiple = 10f;

    public static float GetExpAtLevel(int level) {
        return Mathf.Pow(level, levelExpPower) * levelExpMultiple;
    }

    public static float GetExpForNextLevel(int level) {
        return GetExpAtLevel(level + 1) - GetExpAtLevel(level);
    }

    public static int GetLevelAtExp(float exp) {
        return Mathf.FloorToInt(Mathf.Pow(exp / levelExpMultiple, 1 / levelExpPower));
    }

}
