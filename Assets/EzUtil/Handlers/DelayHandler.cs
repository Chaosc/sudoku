﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayHandler : BaseHandler {

    [SerializeField]
    private UnityEvent onDelay = new UnityEvent();

    public void InvokeDelay(float delay) {
        Invoke("OnDelay", delay);
    }

    void OnDelay() {
        onDelay.Invoke();
    }
}
