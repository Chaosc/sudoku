﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BehaviourHandler : BaseHandler {

    [SerializeField]
    private UnityEvent onAwake = new UnityEvent();

    [SerializeField]
    private UnityEvent onStart = new UnityEvent();

    [SerializeField]
    private UnityEvent onEnable = new UnityEvent();

    [SerializeField]
    private UnityEvent onDisable = new UnityEvent();

    [SerializeField]
    private UnityEvent onDestroy = new UnityEvent();

    void Awake() {
        onAwake.Invoke();
    }

    void Start() {
        onStart.Invoke();
    }

    void OnEnable() {
        onEnable.Invoke();
    }

    void OnDisable() {
        onDisable.Invoke();
    }

    void OnDestroy() {
        onDestroy.Invoke();
    }

}
