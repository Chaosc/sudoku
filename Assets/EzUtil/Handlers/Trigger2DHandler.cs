﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TriggerEnter2DEvent : UnityEvent<Collider2D> { }

[System.Serializable]
public class TriggerStay2DEvent : UnityEvent<Collider2D> { }

[System.Serializable]
public class TriggerExit2DEvent : UnityEvent<Collider2D> { }

public class Trigger2DHandler : BaseHandler {

    public TriggerEnter2DEvent onTriggerEnter2D;
    [SerializeField]
    public LayerMask enterLayerMask;
    public bool enterTriggerOnce = false;

    public TriggerStay2DEvent onTriggerStay2D;
    [SerializeField]
    public LayerMask stayLayerMask;
    public bool stayTriggerOnce = false;

    public TriggerExit2DEvent onTriggerExit2D;
    [SerializeField]
    public LayerMask exitLayerMask;
    public bool exitTriggerOnce = false;

    private bool enterTriggered = false;
    private bool stayTriggered = false;
    private bool exitTriggered = false;

    void OnTriggerEnter2D(Collider2D collider) {
        if (!enterTriggered && onTriggerEnter2D != null && ((1 << collider.gameObject.layer) & enterLayerMask.value) != 0) {
            enterTriggered = enterTriggerOnce;
            onTriggerEnter2D.Invoke(collider);
        }
    }

    void OnTriggerStay2D(Collider2D collider) {
        if (!stayTriggered && onTriggerStay2D != null && ((1 << collider.gameObject.layer) & stayLayerMask.value) != 0) {
            stayTriggered = stayTriggerOnce;
            onTriggerStay2D.Invoke(collider);
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        if (!exitTriggered && onTriggerExit2D != null && ((1 << collider.gameObject.layer) & exitLayerMask.value) != 0) {
            exitTriggered = exitTriggerOnce;
            onTriggerExit2D.Invoke(collider);
        }
    }
}
