﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CollisionEnter2DEvent : UnityEvent<Collision2D> { }

[System.Serializable]
public class CollisionStay2DEvent : UnityEvent<Collision2D> { }

[System.Serializable]
public class CollisionExit2DEvent : UnityEvent<Collision2D> { }

public class Collision2DHandler : BaseHandler {

    public CollisionEnter2DEvent onCollisionEnter2D;
    [SerializeField]
    public LayerMask enterLayerMask;
    public bool enterTriggerOnce = false;

    public CollisionStay2DEvent onCollisionStay2D;
    [SerializeField]
    public LayerMask stayLayerMask;
    public bool stayTriggerOnce = false;

    public CollisionExit2DEvent onCollisionExit2D;
    [SerializeField]
    public LayerMask exitLayerMask;
    public bool exitTriggerOnce = false;

    private bool enterTriggered = false;
    private bool stayTriggered = false;
    private bool exitTriggered = false;

    void OnCollisionEnter2D(Collision2D collision) {
        if (!enterTriggered && onCollisionEnter2D != null && ((1 << collision.gameObject.layer) & enterLayerMask.value) != 0) {
            enterTriggered = enterTriggerOnce;
            onCollisionEnter2D.Invoke(collision);
        }
    }

    void OnCollisionStay2D(Collision2D collision) {
        if (!stayTriggered && onCollisionStay2D != null && ((1 << collision.gameObject.layer) & stayLayerMask.value) != 0) {
            stayTriggered = stayTriggerOnce;
            onCollisionStay2D.Invoke(collision);
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        if (!exitTriggered && onCollisionExit2D != null && ((1 << collision.gameObject.layer) & exitLayerMask.value) != 0) {
            exitTriggered = exitTriggerOnce;
            onCollisionExit2D.Invoke(collision);
        }
    }
}
