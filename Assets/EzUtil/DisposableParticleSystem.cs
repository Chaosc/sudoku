﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisposableParticleSystem : MonoBehaviour {

    public bool destroyWhenStopped = false;

    private ParticleSystem ps;

    void Awake() {
        ps = GetComponent<ParticleSystem>();
    }
	
	void Update() {
		if (!ps.IsAlive()) {
            if (destroyWhenStopped) {
                Destroy(gameObject);
            } else {
                ps.gameObject.SetActive(false);
            }
        }
	}
}
