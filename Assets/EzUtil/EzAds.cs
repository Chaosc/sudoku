﻿using System.Collections.Generic;
using UnityEngine;

public class EzAds {
    public const bool debugInEditor = true;

    public static bool initialized = false;

    private static Dictionary<string, AdSource> supportedSources;
    private static readonly string[] dummySources = { "dummy" };
    private static readonly string[] defaultSources = {
        AdMobSource.instance.name,
#if UNITY_ADS
        UnityAdsSource.instance.name,
#endif
        VungleSource.instance.name
    };
    private static bool interstitialSourcesPolling = false;
    private static bool rewardedVideoSourcesPolling = false;
    private static bool bannerSourcesPolling = false;
    private static List<string> interstitialSources;
    private static List<string> rewardedVideoSources;
    private static List<string>bannerSources;
    private static HashSet<string> totalSources;

    public static void Initialize(string[] interstitialSources, string[] rewardedVideoSources,string[] bannerSources) {
        Debug.Log("EzAds Initializing...");
        if (interstitialSources == null || interstitialSources.Length == 0 || 
            string.IsNullOrEmpty(interstitialSources[0])) {
            interstitialSources = defaultSources;
            interstitialSourcesPolling = true;
        }
        if (rewardedVideoSources == null || rewardedVideoSources.Length == 0 ||
            string.IsNullOrEmpty(rewardedVideoSources[0])) {
            rewardedVideoSources = defaultSources;
            rewardedVideoSourcesPolling = true;
        }
        if (bannerSources == null || bannerSources.Length == 0 ||
           string.IsNullOrEmpty(bannerSources[0])) {
            bannerSources = defaultSources;
            bannerSourcesPolling = true;
        }

        supportedSources = new Dictionary<string, AdSource>();
        if (Application.isEditor) {
            AddSource(DummySource.instance);
            EzAds.interstitialSources = new List<string>(dummySources);
            EzAds.rewardedVideoSources = new List<string>(dummySources);
            EzAds.bannerSources = new List<string>(dummySources);
        } else {
            AddSource(AdMobSource.instance);
#if UNITY_ADS
            AddSource(UnityAdsSource.instance);
#endif
            AddSource(VungleSource.instance);
            EzAds.interstitialSources = new List<string>(interstitialSources);
            EzAds.rewardedVideoSources = new List<string>(rewardedVideoSources);
            EzAds.bannerSources = new List<string>(bannerSources);
        }
        totalSources = new HashSet<string>();
        foreach (string source in interstitialSources) {
            if (!totalSources.Contains(source)) totalSources.Add(source);
        }
        foreach (string source in rewardedVideoSources) {
            if (!totalSources.Contains(source)) totalSources.Add(source);
        }
        foreach (string source in bannerSources) {
            if (!totalSources.Contains(source)) totalSources.Add(source);
        }
        foreach (string source in totalSources) {
            if (supportedSources.ContainsKey(source) && supportedSources[source] != null) {
                supportedSources[source].Initialize();
            }
        }
        initialized = true;
    }

    private static void AddSource(AdSource source) {
        if (!supportedSources.ContainsKey(source.name)) {
            Debug.Log("add global ad source: " + source.name);
            supportedSources.Add(source.name, source);
        }
    }

    public static bool IsInterstitialReady() {
        if (!initialized) return false;
        foreach (string source in interstitialSources) {
            if (supportedSources.ContainsKey(source) && supportedSources[source] != null &&
                supportedSources[source].IsInterstitialReady()) {
                return true;
            }
        }
        return false;
    }

    public static bool ShowInterstitial(System.Action onClose = null) {
        if (!initialized) return false;
        EzAnalytics.LogEvent("Ads", "Interstitial", "Check");
        string sources = "";
        foreach (string source in interstitialSources) sources += source + ",";
        Debug.Log("ShowInterstitial: " + sources.TrimEnd(','));
        foreach (string source in interstitialSources) {
            if (supportedSources.ContainsKey(source) && supportedSources[source] != null &&
                supportedSources[source].ShowInterstitial(onClose)) {
                EzAnalytics.LogEvent("Ads", "Interstitial", "Show");
                if (interstitialSourcesPolling) {
                    interstitialSources.Remove(source);
                    interstitialSources.Add(source);
                }
                return true;
            }
        }
        return false;
    }

    public static bool IsRewardedVideoReady() {
        if (!initialized) return false;
        foreach (string source in rewardedVideoSources) {
            if (supportedSources.ContainsKey(source) && supportedSources[source] != null &&
                supportedSources[source].IsRewardedVideoReady()) {
                return true;
            }
        }
        return false;
    }

    public static bool ShowRewardedVideo(System.Action onRewarded, System.Action onCanceled = null) {
        if (!initialized) return false;
        EzAnalytics.LogEvent("Ads", "RewardedVideo", "Check");
        string sources = "";
        foreach (string source in rewardedVideoSources) sources += source + ",";
        Debug.Log("ShowRewardedVideo: " + sources.TrimEnd(','));
        foreach (string source in rewardedVideoSources) {
            if (supportedSources.ContainsKey(source) && supportedSources[source] != null &&
                supportedSources[source].ShowRewardedVideo(onRewarded, onCanceled)) {
                EzAnalytics.LogEvent("Ads", "RewardedVideo", "Show");
                if (rewardedVideoSourcesPolling) {
                    rewardedVideoSources.Remove(source);
                    rewardedVideoSources.Add(source);
                }
                return true;
            }
        }
        return false;
    }

    public static bool ShowBanner() {
        if (!initialized) return false;
        if (GameManager.instance.noAds) return false;
        EzAnalytics.LogEvent("Ads", "Banner", "Check");
        string sources = "";
        foreach (string source in bannerSources) sources += source + ",";
        Debug.Log("ShowBanner: " + sources.TrimEnd(','));
        return supportedSources[bannerSources[0]].ShowBannerView();
    }

    public static void HideBanner() {
        supportedSources[bannerSources[0]].HideBannerView();
    }

    public static void OnPause() {
        if (!initialized) return;
        foreach (string source in totalSources) {
            if (supportedSources.ContainsKey(source) && supportedSources[source] != null) {
                supportedSources[source].OnPause();
            }
        }
    }

    public static void OnResume() {
        if (!initialized) return;
        foreach (string source in totalSources) {
            if (supportedSources.ContainsKey(source) && supportedSources[source] != null) {
                supportedSources[source].OnResume();
            }
        }
    }
}
