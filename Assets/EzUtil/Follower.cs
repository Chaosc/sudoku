﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour {

    public Transform target;
    public Vector3 offset;
    public bool followX = true;
    public bool followY = true;
    public bool followZ = true;
    public bool holdStartOffset = true;

	// Use this for initialization
	void Start() {
        if (holdStartOffset) {
            offset = transform.position - target.position;
        }
    }
	
	// Update is called once per frame
	void Update() {
        Vector3 position = target.position + offset;
        Vector3 cameraPosition = transform.position;
        if (followX) cameraPosition.x = position.x;
        if (followY) cameraPosition.y = position.y;
        if (followZ) cameraPosition.y = position.z;
        transform.position = cameraPosition;
    }

    void FixedUpdate() {
    }
}
