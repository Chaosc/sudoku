﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class EzDevice {

	private const string UNIQUE_ID = "UniqueID";

	public static string platform {
		get {
			switch (Application.platform) {
			case RuntimePlatform.Android:
				return "Android";
			case RuntimePlatform.IPhonePlayer:
				return "iOS";
			case RuntimePlatform.WebGLPlayer:
				return "WebGL";
			case RuntimePlatform.WindowsPlayer:
				return "Windows";
			case RuntimePlatform.LinuxPlayer:
				return "Linux";
			case RuntimePlatform.OSXPlayer:
				return "OSX";
			case RuntimePlatform.PS4:
				return "PS4";
			case RuntimePlatform.PSM:
				return "PSM";
			case RuntimePlatform.PSP2:
				return "PSP2";
			case RuntimePlatform.WiiU:
				return "Wii";
			case RuntimePlatform.XboxOne:
				return "Xbox";
			case RuntimePlatform.tvOS:
				return "tvOS";
			case RuntimePlatform.TizenPlayer:
				return "Tizon";
			case RuntimePlatform.WindowsEditor:
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.LinuxEditor:
				return "Editor";
			}
			return "Unknown";
		}
	}

	public static bool noNetwork {
		get { return Application.internetReachability == NetworkReachability.NotReachable; }
	}

	public static bool hasNetwork {
		get { return isWifi || isCarrierData; }
	}

	public static bool isWifi {
		get { return Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork; }
	}

	public static bool isCarrierData {
		get { return Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork; }
	}

	public static string GetUniqueID() {
		string id = EzPrefs.GetString(UNIQUE_ID, "");
        if (!string.IsNullOrEmpty(id)) {
            return id;
        }
		if (Application.platform == RuntimePlatform.Android) {
#if UNITY_ANDROID
            try {
                AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject resolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
                AndroidJavaClass secureClass = new AndroidJavaClass("android.provider.Settings$Secure");
                id = secureClass.CallStatic<string>("getString", resolver, secureClass.CallStatic<string>("ANDROID_ID"));
            } catch (AndroidJavaException ex) {
                Debug.LogWarning("Get device id failed :" + ex.Message);
            }
#endif
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
#if UNITY_IOS
            try {
			    id = getUniqueID();
            } catch (Exception ex) {
                Debug.LogWarning("Get device id failed :" + ex.Message);
            }
#endif
        }
        if (string.IsNullOrEmpty(id)) {
            id = Guid.NewGuid().ToString();
            Debug.Log("Generate uuid for unique id of this device");
        }
        EzPrefs.SetString(UNIQUE_ID, id);
		return id;
	}

	public static string GetUniqueIDInMD5() {
		return EzCrypto.MD5(GetUniqueID());
	}

	public static float GetBatteryLevel() {
		float level = 1;
		if (Application.platform == RuntimePlatform.Android) {
#if UNITY_ANDROID
			try {
				string CapacityString = File.ReadAllText("/sys/class/power_supply/battery/capacity");
				level = int.Parse(CapacityString) / 100.0f;
			} catch (Exception e) {
				Debug.Log("Failed to read android battery level; " + e.Message);
			}
#endif
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
#if UNITY_IOS
			level = getBatteryLevel();
#endif
		}
		return level;
	}

	public static bool CheckMicrophone() {
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
#if UNITY_IOS
			return checkMicrophone();
#endif
		}
		return true;
	}

#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern string getUniqueID();

	[DllImport("__Internal")]
	private static extern float getBatteryLevel();

	[DllImport("__Internal")]
	private static extern bool checkMicrophone();
#endif
}
