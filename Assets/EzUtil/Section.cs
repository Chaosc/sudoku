﻿public class Section<T, K> {
    public T value;
    public K threshold;

    public static T GetValue(Section<T, K>[] sections, K key, System.Func<K, K, bool> condition, T defVal = default(T)) {
        foreach (Section<T, K> section in sections) {
            if (condition(key, section.threshold)) {
                return section.value;
            }
        }
        return defVal;
    }
}

public static class IntCondition {
    public static bool Equal(int one, int two) {
        return one == two;
    }

    public static bool Less(int one, int two) {
        return one < two;
    }

    public static bool Greater(int one, int two) {
        return one > two;
    }

    public static bool NoLess(int one, int two) {
        return one >= two;
    }

    public static bool NoGreater(int one, int two) {
        return one <= two;
    }
}

public static class FloatCondition {
    public static bool Equal(float one, float two) {
        return one == two;
    }

    public static bool Less(float one, float two) {
        return one < two;
    }

    public static bool Greater(float one, float two) {
        return one > two;
    }

    public static bool NoLess(float one, float two) {
        return one >= two;
    }

    public static bool NoGreater(float one, float two) {
        return one <= two;
    }
}