﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzTransformPools<P> : Singleton<P> where P : Component {

    protected EzTransformPool[] pools;
    protected float[] chances;

    protected virtual void Awake() {
        pools = GetComponentsInChildren<EzTransformPool>(false);
        chances = new float[pools.Length];
        for (int i = 0; i < pools.Length; ++i) {
            chances[i] = pools[i].chance;
        }
    }

    public Transform Create(int id) {
        return pools[id % pools.Length].Create();
    }

    public T Create<T>(int id) where T : Component {
        return pools[id % pools.Length].Create<T>();
    }

    public Transform CreateRandom() {
        int index = EzRandom.Roll(chances);
        return pools[index].Create();
    }

    public T CreateRandom<T>() where T : Component {
        int index = EzRandom.Roll(chances);
        return pools[index].Create<T>();
    }

    public virtual void Recycle(Transform child) {
        child.GetComponentInParent<EzTransformPool>().Recycle(child);
    }
}
