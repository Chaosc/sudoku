using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VungleSource : AdSource {

#if UNITY_IPHONE
    private const string appId = "5b584592202fe30c8df2ec4e";
    private const string interstitialPlacement = "_____IOS___-4205950";
    private const string rewardedVideoPlacement = "_____IOS___-3967379";
#elif UNITY_ANDROID
    private const string appId = "5b60595324475f0ca545c540";
    private const string interstitialPlacement = "___ANDROID___-0989118";
    private const string rewardedVideoPlacement = "___ANDROID___-7221115";
#else
    private const string appId = "";
    private const string interstitialPlacement = "";
    private const string rewardedVideoPlacement = "";
#endif

    private int orientation {
        get {
            switch (Screen.orientation) {
                case ScreenOrientation.Portrait: return 1;
                case ScreenOrientation.PortraitUpsideDown: return 4;
                case ScreenOrientation.Landscape: return 5;
                case ScreenOrientation.LandscapeRight: return 3;
            }
            return 6;
        }
    }

    private Action onInterstitialClose;
    private Action onRewardedVideoFinish;
    private Action onRewardedVideoCancel;

    public string name { get { return "vungle"; } }

    public static VungleSource instance = new VungleSource();

    private VungleSource() { }

    public void Initialize() {
        Vungle.onAdFinishedEvent += OnAdFinished;
        Vungle.init(appId, new string[] { interstitialPlacement, rewardedVideoPlacement });
        Debug.Log("[Vungle] initialized");
    }

    public bool IsInterstitialReady() {
        return Vungle.isAdvertAvailable(interstitialPlacement);
    }

    public bool ShowInterstitial(Action onClose = null) {
        EzAnalytics.LogEvent("Vungle", "Interstitial", "Check");
        if (IsInterstitialReady()) {
            onInterstitialClose = onClose;
            Debug.Log("[Vungle] show interstitial");
            EzAnalytics.LogEvent("Vungle", "Interstitial", "Show");
            EzAnalytics.LogEvent("Interstitial", "Show", "Vungle");
            Vungle.playAd(interstitialPlacement);
            return true;
        }
        Debug.Log("[Vungle] interstitial is not ready");
        return false;
    }

    public bool IsRewardedVideoReady() {
        return Vungle.isAdvertAvailable(rewardedVideoPlacement);
    }

    public bool ShowRewardedVideo(Action onRewarded, Action onCanceled = null) {
        EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Check");
        if (IsRewardedVideoReady()) {
            onRewardedVideoFinish = onRewarded;
            onRewardedVideoCancel = onCanceled;
            Debug.Log("[Vungle] show rewarded video");
            EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Show");
            EzAnalytics.LogEvent("RewardedVideo", "Show", "Vungle");
            Vungle.playAd(rewardedVideoPlacement);
            return true;
        }
        Debug.Log("[Vungle] rewarded video is not ready");
        return false;
    }

    void OnAdFinished(string finishStr, AdFinishedEventArgs args) {
        if (args.IsCompletedView && onRewardedVideoFinish != null) {
            Debug.Log("[Vungle] rewarded video completed"+ finishStr);
            EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Finished" + finishStr);
            onRewardedVideoFinish.Invoke();
            onRewardedVideoFinish = null;
        } else if (onRewardedVideoCancel != null) {
            Debug.LogWarning("[Vungle] rewarded video was skipped" + finishStr);
            EzAnalytics.LogEvent("Vungle", "RewardedVideo", "Skipped" + finishStr);
            onRewardedVideoCancel.Invoke();
            onRewardedVideoCancel = null;
        } else if (onInterstitialClose != null) {
            Debug.Log("[Vungle] interstitial completed" + finishStr);
            EzAnalytics.LogEvent("Vungle", "Interstitial", "Closed" + finishStr);
            onInterstitialClose.Invoke();
            onInterstitialClose = null;
        }
    }

    public bool ShowBannerView() {
        throw new NotImplementedException();
    }

    public void OnPause() {
        Vungle.onPause();
    }

    public void OnResume() {
        Vungle.onResume();
    }

    public bool HideBannerView() {
        throw new NotImplementedException();
    }
}
