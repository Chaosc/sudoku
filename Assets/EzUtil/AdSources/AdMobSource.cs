﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdMobSource : AdSource {

    public bool autoReloadInterstitialOnFailed = true;
    public bool autoReloadInterstitialOnClosed = true;

    public bool autoReloadBannerOnFailed = true;
    public bool autoReloadBannerOnClosed = true;

    public bool autoReloadRewardedVideoOnFailed = true;
    public bool autoReloadRewardedVideoOnClosed = true;

    public float autoReloadDelay = 2;

    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardedVideo;
    private BannerView bannerView;

    private bool requestingInterstitial = false;
    private bool requestingRewardedVideo = false;
    private bool requestBanner = false;

    private System.Action rewardedAction;
    private System.Action canceledAction;
    private System.Action closeAction;

    private bool rewarded = false;

    public string name { get { return "admob"; } }

    public static AdMobSource instance = new AdMobSource();

    private AdMobSource() {}

    public void Initialize() {
        RequestInterstitial();
        RequestRewardedVideo();
        Debug.Log("[AdMob] initialized");
    }
    #region bannar
    public IEnumerator RequestBannerDelayed(float delay) {
        yield return new WaitForSeconds(delay);
        RequestBannar();
    }

    public void RequestBannar() {

        string adUnitId = EzRemoteConfig.adBannerId;
        if (string.IsNullOrEmpty(adUnitId)) {
            Debug.LogWarning("[AdMob] banner unit id is null");
            return;
        }
        if (requestBanner) {
            Debug.Log("[AdMob] loading banner, please wait.");
            return;
        }
        requestBanner = true;
        if (bannerView != null) bannerView.Destroy();
        // Initialize an InterstitialAd.


        bannerView = new BannerView(adUnitId,AdSize.Banner,AdPosition.Bottom);
        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += HandleOnBannerAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleOnBannerAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.OnAdOpening += HandleOnBannerAdOpening;
        // Called when the user returned from the app after an ad click.
        bannerView.OnAdClosed += HandleOnBannerAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.OnAdLeavingApplication += HandleOnBannerAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        Debug.Log("[AdMob] loading banner...");
        bannerView.LoadAd(request);
    }

    public bool ShowBanner() {
        string adUnitId = EzRemoteConfig.adBannerId;
        if (string.IsNullOrEmpty(adUnitId)) {
            Debug.LogWarning("[AdMob] banner unit id is null");
            return false;
        }
        if (requestBanner) {
            Debug.Log("[AdMob] loading banner, please wait.");
            return false;
        }
        requestBanner = true;
        if (bannerView != null) {
            bannerView.Show();
            return true;
        }
        // Initialize an InterstitialAd.
        

        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += HandleOnBannerAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleOnBannerAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.OnAdOpening += HandleOnBannerAdOpening;
        // Called when the user returned from the app after an ad click.
        bannerView.OnAdClosed += HandleOnBannerAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.OnAdLeavingApplication += HandleOnBannerAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        Debug.Log("[AdMob] loading banner...");
        bannerView.LoadAd(request);
     
        return true;
    }

    IEnumerator CheckBannerTimeout(float delay) {
        BannerView ad = bannerView;
        yield return new WaitForSeconds(delay);
        Debug.Log("[AdMob] Banner loaded after 120 seconds.");
        if (ad == bannerView) {
            Debug.Log("[AdMob] Banner out date!");
            bannerView.Destroy();
            bannerView = null;
            requestBanner = false;
        }
    }

    public void HandleOnBannerAdLoaded(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] Banner loaded.");
        EzAnalytics.LogEvent("AdMob", "Banner", "Loaded");
        GameManager.instance.StartCoroutine(CheckBannerTimeout(120));
    }

    public void HandleOnBannerAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        Debug.Log("[AdMob] Banner failed to load: " + args.Message);
        EzAnalytics.LogEvent("AdMob", "Banner", "Failed");
        requestBanner = false;
        if (autoReloadBannerOnFailed) RequestBannar();
    }

    public void HandleOnBannerAdOpening(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] Banner opening.");
        EzAnalytics.LogEvent("AdMob", "Banner", "Open");
        requestBanner = false;
    }

    public void HandleOnBannerAdClosed(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] Banner closed.");
        EzAnalytics.LogEvent("AdMob", "Banner", "Close");
        if (autoReloadBannerOnClosed) {
            GameManager.instance.StartCoroutine(RequestBannerDelayed(autoReloadDelay));
        }
    }

    public void HandleOnBannerAdLeavingApplication(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] user left after clicked Banner.");
        EzAnalytics.LogEvent("AdMob", "Banner", "Click");
    }





    #endregion


    #region interstitial

    public IEnumerator RequestInterstitialDelayed(float delay) {
        yield return new WaitForSeconds(delay);
        RequestInterstitial();
    }

    public void RequestInterstitial() {
        if (IsInterstitialReady()) return;

        string adUnitId = EzRemoteConfig.adInterstitialId;
        if (string.IsNullOrEmpty(adUnitId)) {
            Debug.LogWarning("[AdMob] Interstitial unit id is null");
            return;
        }
        if (requestingInterstitial) {
            Debug.Log("[AdMob] loading Interstitial, please wait.");
            return;
        }
        requestingInterstitial = true;
        if (interstitial != null) interstitial.Destroy();
        // Initialize an InterstitialAd.
    

        interstitial = new InterstitialAd(adUnitId);
        // Called when an ad request has successfully loaded.
        interstitial.OnAdLoaded += HandleOnInterstitialAdLoaded;
        // Called when an ad request failed to load.
        interstitial.OnAdFailedToLoad += HandleOnInterstitialAdFailedToLoad;
        // Called when an ad is clicked.
        interstitial.OnAdOpening += HandleOnInterstitialAdOpening;
        // Called when the user returned from the app after an ad click.
        interstitial.OnAdClosed += HandleOnInterstitialAdClosed;
        // Called when the ad click caused the user to leave the application.
        interstitial.OnAdLeavingApplication += HandleOnInterstitialAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        Debug.Log("[AdMob] loading interstitial...");
        interstitial.LoadAd(request);
    }

    public bool ShowInterstitial(System.Action onClose = null) {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor) {
            return true;
        }
        EzAnalytics.LogEvent("AdMob", "Interstitial", "Check");
        if (IsInterstitialReady()) {
            closeAction = onClose;
            Debug.Log("[AdMob] show interstitial");
            EzAnalytics.LogEvent("AdMob", "Interstitial", "Show");
            EzAnalytics.LogEvent("Interstitial", "Show", "AdMob");
            interstitial.Show();
            return true;
        }
        Debug.Log("[AdMob] interstitial is not ready");
        RequestInterstitial();
        return false;
    }

    public bool IsInterstitialReady() {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor) {
            return true;
        }
        return (interstitial != null && interstitial.IsLoaded());
    }

    IEnumerator CheckIntersitialTimeout(float delay) {
        InterstitialAd ad = interstitial;
        yield return new WaitForSeconds(delay);
        Debug.Log("[AdMob] interstitial loaded after 120 seconds.");
        if (ad == interstitial) {
            Debug.Log("[AdMob] interstitial out date!");
            interstitial.Destroy();
            interstitial = null;
            requestingInterstitial = false;
        }
    }

    public void HandleOnInterstitialAdLoaded(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] interstitial loaded.");
        EzAnalytics.LogEvent("AdMob", "Interstitial", "Loaded");
        GameManager.instance.StartCoroutine(CheckIntersitialTimeout(120));
    }

    public void HandleOnInterstitialAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        Debug.Log("[AdMob] interstitial failed to load: " + args.Message);
        EzAnalytics.LogEvent("AdMob", "Interstitial", "Failed");
        requestingInterstitial = false;
        if (autoReloadInterstitialOnFailed) RequestInterstitial();
    }

    public void HandleOnInterstitialAdOpening(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] interstitial opening.");
        EzAnalytics.LogEvent("AdMob", "Interstitial", "Open");
        requestingInterstitial = false;
    }

    public void HandleOnInterstitialAdClosed(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] interstitial closed.");
        EzAnalytics.LogEvent("AdMob", "Interstitial", "Close");
        if (autoReloadInterstitialOnClosed) {
            GameManager.instance.StartCoroutine(RequestInterstitialDelayed(autoReloadDelay));
        }
    }

    public void HandleOnInterstitialAdLeavingApplication(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] user left after clicked interstitial.");
        EzAnalytics.LogEvent("AdMob", "Interstitial", "Click");
    }

    #endregion

    #region rewardvideo

    public IEnumerator RequestRewardedVideoDelayed(float delay) {
        yield return new WaitForSeconds(delay);
        RequestRewardedVideo();
    }

    public void RequestRewardedVideo() {
        if (rewardedVideo != null && rewardedVideo.IsLoaded()) return;

        string adUnitId = EzRemoteConfig.adRewardedVideoId;
        if (string.IsNullOrEmpty(adUnitId)) {
            Debug.LogWarning("[AdMob] rewarded video unit id is null");
            return;
        }
        if (requestingRewardedVideo) {
            Debug.Log("[AdMob] loading rewarded video, please wait.");
            return;
        }
        requestingRewardedVideo = true;
        if (rewardedVideo == null) {
            // Initialize an RewardedVideoAd.

            rewardedVideo = RewardBasedVideoAd.Instance;
            // Called when an ad request has successfully loaded.
            rewardedVideo.OnAdLoaded += HandleOnRewardedVideoAdLoaded;
            // Called when an ad request failed to load.
            rewardedVideo.OnAdFailedToLoad += HandleOnRewardedVideoAdFailedToLoad;
            // Called when an ad is clicked.
            rewardedVideo.OnAdOpening += HandleOnRewardedVideoAdOpening;
            // has started playing.
            rewardedVideo.OnAdStarted += HandleOnRewardedVideoAdStarted;
            // has rewarded the user.
            rewardedVideo.OnAdRewarded += HandleOnRewardedVideoAdRewarded;
            // Called when the user returned from the app after an ad click.
            rewardedVideo.OnAdClosed += HandleOnRewardedVideoAdClosed;
            // Called when the ad click caused the user to leave the application.
            rewardedVideo.OnAdLeavingApplication += HandleOnRewardedVideoAdLeavingApplication;
            // Create an empty ad request.
        }
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewardVideo with the request.
        Debug.Log("[AdMob] loading rewarded video...");
        rewardedVideo.LoadAd(request, adUnitId);
    }


    public bool ShowRewardedVideo(System.Action onRewarded, System.Action onCanceled = null) {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor) {
            if (onRewarded != null) {
                onRewarded.Invoke();
            }
            return true;
        }
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Check");
        if (rewardedVideo != null && rewardedVideo.IsLoaded()) {
            rewardedAction = onRewarded;
            canceledAction = onCanceled;
            Debug.Log("[AdMob] show rewarded video");
            EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Show");
            EzAnalytics.LogEvent("RewardedVideo", "Show", "AdMob");
            rewardedVideo.Show();
            return true;
        }
        Debug.Log("[AdMob] rewarded video is not ready");
        RequestRewardedVideo();
        return false;
    }

    public bool IsRewardedVideoReady() {
        return rewardedVideo != null && rewardedVideo.IsLoaded();
    }

    public void HandleOnRewardedVideoAdLoaded(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] rewarded video loaded. " + rewardedVideo.IsLoaded());
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Loaded");
    }

    public void HandleOnRewardedVideoAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        Debug.Log("[AdMob] rewarded video failed to load: " + args.Message);
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Failed");
        requestingRewardedVideo = false;
        if (autoReloadRewardedVideoOnFailed) RequestRewardedVideo();
    }

    public void HandleOnRewardedVideoAdOpening(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] rewarded video opening.");
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Open");
        requestingRewardedVideo = false;
    }

    public void HandleOnRewardedVideoAdStarted(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] rewarded video started.");
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Start");
    }

    public void HandleOnRewardedVideoAdRewarded(object sender, Reward args) {
        Debug.Log("[AdMob] rewarded video rewarded: type=" + args.Type + ", amount=" + args.Amount);
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Reward");
        rewarded = true;
    }

    public void HandleOnRewardedVideoAdClosed(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] rewarded video closed.");
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Close");
        if (rewarded) {
            rewarded = false;
            if (rewardedAction != null) {
                rewardedAction.Invoke();
            }
        } else {
            if (canceledAction != null) {
                canceledAction.Invoke();
            }
        }
        if (autoReloadRewardedVideoOnClosed) {
            GameManager.instance.StartCoroutine(RequestRewardedVideoDelayed(autoReloadDelay));
        }
    }

    public void HandleOnRewardedVideoAdLeavingApplication(object sender, System.EventArgs args) {
        Debug.Log("[AdMob] user left after clicked rewarded video.");
        EzAnalytics.LogEvent("AdMob", "RewardedVideo", "Click");
    }
    #endregion

    public void OnPause() {
    }

    public void OnResume() {
    }

    public bool ShowBannerView() {
        return ShowBanner();
    }

    public bool HideBannerView() {
        if (bannerView == null) return false;
        bannerView.Hide();
        return true;
    }
}
