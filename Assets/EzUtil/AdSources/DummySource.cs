﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummySource : AdSource {
    public string name { get { return "dummy"; } }

    public static DummySource instance = new DummySource();

    private DummySource() { }

    public void Initialize() {
        Debug.Log("[DummySource] initialized");
    }

    public bool IsInterstitialReady() {
        return true;
    }

    public bool ShowInterstitial(Action onClose = null) {
        if (onClose != null) {
            onClose.Invoke();
        }
        return true;
    }

    public bool IsRewardedVideoReady() {
        return true;
    }

    public bool ShowRewardedVideo(Action onRewarded, Action onCanceled = null) {
        if (onRewarded != null) {
            onRewarded.Invoke();
        }
        return true;
    }

    public void OnPause() {
    }

    public void OnResume() {
    }

    public bool ShowBannerView() {
        throw new NotImplementedException();
    }
    public bool HideBannerView() {
        throw new NotImplementedException();
    }
}
