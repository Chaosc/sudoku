﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EzRandomSpawner<T, P> : EzTransformPools<P> where T : Component where P : Component {

    public float delay;
    public float interval = 1f;
    public int spawnCount = 1;
    public int maxCount = -1;
    public float probability = 1f;
    public bool spawnOnStart = true;
    public bool running = false;
    public bool paused = false;

    [System.Serializable]
    public class SpawnEvent : UnityEvent<T> { }

    public SpawnEvent onSpawn;

    protected int count = 0;
    protected float spawnTime;
    protected float elapsedTime;

    protected virtual void Start() {
        spawnTime = delay;
        running = spawnOnStart;
    }

    protected virtual void Update() {
        if (running) {
            if (elapsedTime < spawnTime) {
                elapsedTime += Time.deltaTime;
            } else if (TrySpawn()) {
                elapsedTime -= spawnTime;
                spawnTime = interval;
            }
        }
    }

    protected virtual bool TrySpawn() {
        if (paused) {
            return false;
        }
        for (int i = 0; i < spawnCount; ++i) {
            if (CanSpawn()) {
                Spawn();
            }
        }
        return true;
    }

    public virtual T Spawn() {
        T obj = CreateRandom<T>();
        if (obj != null) {
            ++count;
            OnSpawn(obj);
        }
        return obj;
    }

    protected virtual bool CanSpawn() {
        return (maxCount <= 0 || count < maxCount) && (Random.value < probability);
    }

    protected virtual void OnSpawn(T obj) {
        if (onSpawn != null) {
            onSpawn.Invoke(obj);
        }
    }

    public override void Recycle(Transform child) {
        base.Recycle(child);
        --count;
    }
}
