﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;

public class GameController : Singleton<GameController> {

    public Text timeText;
    public float timeF;
    private bool isPause = false;
    public SDK9X9 sDK9X9;
    public Text TitleText;
    public GameObject hintVideo;
    public GameObject hintQipao;
    public GameObject pencilHintIcon;
    public GameObject rmHintIcon;
    public GameObject pencilHint;
    public GameObject rmHint;

    public float OverTime = 360;

    public GameObject PauseGo;

    public GameObject GameOverImage;


    public Image pencilImage;

    public Text hintNum;
    public RectTransform[] rtfs;

    // Use this for initialization
    void Start() {
        GameManager.instance.OnGameStart();
        string titleStr = "";
        switch(GameManager.instance.currentLevel) {
            case 1:
                titleStr = Localization.GetMultilineText("Diff1");
                break;
            case 3:
                titleStr = Localization.GetMultilineText("Diff2");
                break;
            case 5:
                titleStr = Localization.GetMultilineText("Diff3");
                break;
            case 7:
                titleStr = Localization.GetMultilineText("Diff4");
                break;
            case 9:
                titleStr = Localization.GetMultilineText("Diff5");
                break;
        }
        if (GameManager.instance.isChallenge) {
            TitleText.text = Localization.GetMultilineText("ChallengeMode");
            GameManager.instance.CostMasterNum();
        } else {
            TitleText.text = titleStr + "LV" + GameManager.instance.currentLittleLevel;
        }
        hintNum.text = GameManager.instance.HintNum.ToString();
        timeText.transform.parent.gameObject.SetActive(GameManager.instance.GetTimeBtn());
        hintNum.transform.parent.parent.gameObject.SetActive(GameManager.instance.GetHintBtn());
        //timeText.gameObject.SetActive(GameManager.instance.GetTimeBtn());
        if (GameManager.instance.HintNum <= 0) {
            if (EzAds.IsRewardedVideoReady()) {
                hintVideo.SetActive(true);
                hintNum.gameObject.SetActive(false);
                ShowHintQipao();
            }
        }

        pencilHintIcon.SetActive(!GameManager.instance.IsShowPenculHint());
        rmHintIcon.SetActive(!GameManager.instance.IsShowRmHint());
        EzAds.ShowBanner();

        if (Screen.width == 1125 && Screen.height == 2436) {
            foreach(RectTransform rtf in rtfs) {
                rtf.offsetMin = new Vector2(0, 44f);
                rtf.offsetMax = new Vector2(0, -44f);
            }
           
        }
    }

    // Update is called once per frame
    float timeChallenge;
    void Update() {
        Panel.CheckBackButton(() => GameManager.instance.LoadScene("Home"));
        if(!isPause) {
            timeF += Time.deltaTime;
        }
        


        if (GameManager.instance.isChallenge && timeF >= OverTime) {
            //GameOver();
            Panel.Open("FailSettlementPanel");
            GameManager.instance.isChallenge = false;
        }
            
        if (GameManager.instance.isChallenge) {
            timeChallenge = OverTime - timeF;
            timeText.text = CountTime((long)timeChallenge);
        } else {
            timeText.text = CountTime((long)timeF);
        }
    }

    public void Revive() {
        // Todo: revive player

    }


    public void GameOver() {
        GameManager.instance.UpdateScore((int)timeF);
        if (!GameManager.instance.isChallenge) {
            GameManager.instance.SubPanelCount++;
            if(GameManager.instance.SubPanelCount % 2 == 0 ) {
#if !UNITY_ANDROID
                Panel.Open("SubPanel");
#endif
            }
            GameManager.instance.OnGameOver();
        }else {
            Panel.Open("ChallengelSettlementPanel");
        }
        OverTime = 9999999;
        EzAds.HideBanner();
    }

    public string CountTime(long duration) {
        TimeSpan ts = new TimeSpan(0, 0, Convert.ToInt32(duration));
        string str = "";
        if (ts.Seconds > 9) {
            str = ts.Minutes.ToString() + ":" + ts.Seconds + "";
            if (ts.Hours > 0) {
                str = ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds + "";
            }
        } else {
            str = ts.Minutes.ToString() + ":0" + ts.Seconds + "";
            if (ts.Hours > 0) {
                str = ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":0" + ts.Seconds + "";
            }
        } 
      
      
        return str;
    }


    public void Repeal() {
        sDK9X9.Repeal();
    }

    public void Rm() {
        if (!GameManager.instance.IsShowRmHint()) {
            GameManager.instance.ShowRmHint();
   
            rmHintIcon.SetActive(!GameManager.instance.IsShowRmHint());
            TweenMove tm = rmHint.GetComponent<TweenMove>();
            tm.position = new Vector3(0, 75.3f, 0);
            tm.Play();
            TweenFade tf = rmHint.GetComponent<TweenFade>();
            tf.delay = 0.3f;
            tf.alpha = 1;
            tf.Play();
            EzTiming.CallDelayed(3.5f, () => {
                EzTiming.CallDelayed(0.3f, () => {
                    tm.position = new Vector3(0, -75.3f, 0);
                    tm.Play();
                });
                tf.delay = 0;
                tf.alpha = 0;
                tf.Play();
            });
        }
        sDK9X9.Rm();
    }

    public void Pencil() {
        if (!GameManager.instance.IsShowPenculHint()) {
            GameManager.instance.ShowPenculHint();
            pencilHintIcon.SetActive(!GameManager.instance.IsShowPenculHint());
            TweenMove tm = pencilHint.GetComponent<TweenMove>();
            tm.position = new Vector3(0, 75.3f, 0);
            tm.Play();
            TweenFade tf = pencilHint.GetComponent<TweenFade>();
            tf.delay = 0.3f;
            tf.alpha = 1;
            tf.Play();
            EzTiming.CallDelayed(3.5f, () => {
                EzTiming.CallDelayed(0.3f, () => {
                    tm.position = new Vector3(0, -75.3f, 0);
                    tm.Play();
                });
                tf.delay = 0;
                tf.alpha = 0;
                tf.Play();
            });
        }

        if (sDK9X9.isPencil) {
            pencilImage.color = Color.white;
        } else {
            pencilImage.color = Color.red;
        }
        sDK9X9.Pencil();
    }

    public void Hint() {
        if(GameManager.instance.HintNum > 0) {
            sDK9X9.Hint();
            GameManager.instance.CostHintNum();
            if(GameManager.instance.HintNum <= 0) {
                if (EzAds.IsRewardedVideoReady()) {
                    hintVideo.SetActive(true);
                    hintNum.gameObject.SetActive(false);
                    ShowHintQipao();
                }
            }
            hintNum.text = GameManager.instance.HintNum.ToString();
        } else {
            EzAds.ShowRewardedVideo(() => {
                hintNum.gameObject.SetActive(true);
                hintVideo.SetActive(false);
                HideHintQipao();
                GameManager.instance.GainHintNum(1);
                hintNum.text = GameManager.instance.HintNum.ToString();
            });
        }
     
    }

    private void ShowHintQipao() {
        TweenMove tm = hintQipao.GetComponent<TweenMove>();
        tm.position = new Vector3(0, 75.3f, 0);
        tm.Play();
        TweenFade tf = hintQipao.GetComponent<TweenFade>();
        tf.delay = 0.3f;
        tf.alpha = 1;
        tf.Play();
    }

    private void HideHintQipao() {
        EzTiming.CallDelayed(0.3f, () => {
            TweenMove tm = hintQipao.GetComponent<TweenMove>();
            tm.position = new Vector3(0, -75.3f, 0);
            tm.Play();
        });
  
        TweenFade tf =  hintQipao.GetComponent<TweenFade>();
        tf.delay = 0;
        tf.alpha = 0;
        tf.Play();
    }

    public void CancelPause() {
        isPause = false;
    }


    public void Pause() {
        isPause = true;
        PauseGo.SetActive(true);
        GameManager.instance.PlaySound("pause");
    }

    public void ConnectedPlay() {
        isPause = false;
        PauseGo.SetActive(false);
        GameManager.instance.PlaySound("pause");
    }

    public void BackHome() {
        if (GameManager.instance.isChallenge) {
            Panel.Open("BackConfirmPanel");
            isPause = true;
        } else {
            GameManager.instance.isGoDiffcultSelect = true;
            GameManager.instance.LoadScene("Home");
        }
    }


    public void ShowGameOverImage() {
        GameOverImage.SetActive(true);
    }

    public void HideGameOverImage() {
        GameOverImage.SetActive(false);
    }

    private void OnDestroy() {
        EzAds.HideBanner();
    }


}
 