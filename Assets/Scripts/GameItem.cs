﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItem : MonoBehaviour {
    public int index;
    public GameObject Pencil;

    public bool CanChange = false;
	// Use this for initialization
	void Start () {
        Pencil = gameObject.transform.GetChild(3).gameObject;
    }
	

    
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowPencil(int x) {
        GameObject pencilGo =  Pencil.transform.GetChild(x - 1).gameObject;
        pencilGo.SetActive(!pencilGo.activeSelf);
    }

    public void HindPencel() {
        for(int i = 0;i< Pencil.transform.childCount; i++) {
            Pencil.transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
