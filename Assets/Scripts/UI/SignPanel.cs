﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SignPanel : Panel {

    public Text ChanlleText;
    public Text SignNum;
    bool showHint = false;
    public Image Hint;

    // Use this for initialization
    void Start () {
        if (EzPrefs.GetBool(DateTime.Today.ToString())) {
            if ("CN".Equals(Localization.GetLanguage())) {
                ChanlleText.text = "明天再来";
                SignNum.text = "您已经连续挑战" + GameManager.instance.challengeNum + "天";
            } else {
                ChanlleText.text = "come back tomorrow";
                SignNum.text = "Your have challenge succeed in" + GameManager.instance.challengeNum + "day running.";
            }
        } else {
            if ("CN".Equals(Localization.GetLanguage())) {
                ChanlleText.text = "挑战";
                SignNum.text = "您已经连续挑战" + GameManager.instance.challengeNum + "天";
            } else {
                ChanlleText.text = "Challenge";
                SignNum.text = "Your have challenge succeed in" + GameManager.instance.challengeNum + "day running.";
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame() {
        if (!EzPrefs.GetBool(DateTime.Today.ToString())) {
            GameManager.instance.isChallenge = true;
            GameManager.instance.currentLevel = 1;
            GameManager.instance.currentLittleLevel = 10;
            GameManager.instance.LoadScene("Game");
        }else {
            if ("CN".Equals(Localization.GetLanguage())) {
                Toast.Show("Challenge success, come back tomorrow!");
            }else {
                Toast.Show("挑战成功，明天再来哦！");
            }
            
        }
    }

    public void ClickHint() {
        if (!showHint) {
            EzTiming.CallDelayed(0.3f, () => {
                Hint.DOFade(1, 1f);
            });
         
            TweenMove tm = Hint.GetComponent<TweenMove>();
            tm.position = new Vector3(-269, 0, 0);
            tm.Play();
           
        } else {
            Hint.DOFade(0, 1f);
            TweenMove tm = Hint.GetComponent<TweenMove>();
            tm.position = new Vector3(269, 0, 0);
            tm.Play();
        }
        showHint = !showHint;
    }
}
