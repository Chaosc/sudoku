﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetingPanel : Panel {

    public Sprite OpenBtnSprite;

    public Sprite CloseBtnSprite;

    public Image timeBtn;
    public Image hintBtn;
    public Image chanBtn;



	// Use this for initialization
	void Start () {
        if (GameManager.instance.GetHintBtn()) {
            hintBtn.sprite = OpenBtnSprite;
        } else {
            hintBtn.sprite = CloseBtnSprite;
        }
        if (GameManager.instance.GetTimeBtn()) {
            timeBtn.sprite = OpenBtnSprite;
        } else {
            timeBtn.sprite = CloseBtnSprite;
        }
        if (GameManager.instance.GetChanBtn()) {
            chanBtn.sprite = OpenBtnSprite;
        } else {
            chanBtn.sprite = CloseBtnSprite;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
    
    private void ShowTimeBtn() {
        if (GameManager.instance.GetTimeBtn()) {
            timeBtn.sprite = OpenBtnSprite;
        } else {
            timeBtn.sprite = CloseBtnSprite;
        }
    }

    private void ShowHintBtn() {
        if (GameManager.instance.GetHintBtn()) {
            hintBtn.sprite = OpenBtnSprite;
        } else {
            hintBtn.sprite = CloseBtnSprite;
        }
    }

    private void ShowChanBtn() {
        if (GameManager.instance.GetChanBtn()) {
            chanBtn.sprite = OpenBtnSprite;
        } else {
            chanBtn.sprite = CloseBtnSprite;
        }
    }


    public void ClickTime() {
        GameManager.instance.ChangeTimeBtn();
        ShowTimeBtn();
    }

    public void ClickHint() {
        GameManager.instance.ChangeHintBtn();
        ShowHintBtn();
    }

    public void ClickChan() {
        GameManager.instance.ChangeChanBtn();
        ShowChanBtn();
    }


}
