﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueLevelPanel : Panel {
    public int index;
    private string levelPanel;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GiveUp() {
        GameManager.instance.SetSavaLevel("");
        GameManager.instance.SetSavaLittleLevel("");
        GameManager.instance.currentLittleLevel = index + 1;
        GameManager.instance.LoadScene("Game");
    }

    public void ContinueLevel() {
        int currentLevel = 1;

        switch (GameManager.instance.CheckSavaLevel()) {
            case "A":
                currentLevel = 1;
                break;
            case "B":
                currentLevel = 3;
                break;
            case "C":
                currentLevel = 5;
                break;
            case "D":
                currentLevel = 7;
                break;
            case "E":
                currentLevel = 9;
                break;
        }
        GameManager.instance.currentLevel = currentLevel;
        GameManager.instance.currentLittleLevel = System.Convert.ToInt32(GameManager.instance.CheckSavaLittleLevel());
        GameManager.instance.LoadScene("Game");

    }
}
