﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomePage : SceneController<HomePage> {

    public GameObject SubGo;


    protected override void OnStart() {
        base.OnStart();
        if (GameManager.instance.isGoDiffcultSelect) {
            Panel.Open("DifficultSelectPanel");
        }
        if (GameManager.instance.isGoLevelSelect) {
            Panel.Open("LevelSelectPanel");
        }
        GameManager.instance.isGoDiffcultSelect = false;
        GameManager.instance.isChallenge = false;
        GameManager.instance.PlayMusic();

#if UNITY_ANDROID
        SubGo.SetActive(false);
#endif
    }

    // Update is called once per frame
    void Update () {
		
	}


    public void RePlayAnima() {

    }
}
