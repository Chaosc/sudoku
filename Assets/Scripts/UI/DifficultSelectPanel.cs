﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultSelectPanel : Panel {

    public Sprite UnClearSprite;
    public Text lv1Num;
    public Text lv2Num;
    public Text lv3Num;
    public Text lv4Num;
    public Text lv5Num;
    public GameObject lv4Lock;
    public GameObject lv5Lock;
    // Use this for initialization
    void Start () {
        lv1Num.text = GameManager.instance.dif1Num+"/200";
        lv2Num.text = GameManager.instance.dif2Num + "/200";
        lv3Num.text = GameManager.instance.dif3Num + "/200";
        lv4Num.text = GameManager.instance.dif4Num + "/200";
        lv5Num.text = GameManager.instance.dif5Num + "/200";
      
        if (!GameManager.instance.CheckLevelIsClear("A" + 200)){
            lv1Num.transform.parent.GetComponent<Image>().sprite = UnClearSprite;
        }
        if (!GameManager.instance.CheckLevelIsClear("B" + 200)) {
            lv2Num.transform.parent.GetComponent<Image>().sprite = UnClearSprite;
        }
        if (!GameManager.instance.CheckLevelIsClear("C" + 200)) {
            lv3Num.transform.parent.GetComponent<Image>().sprite = UnClearSprite;
        }
        if (!GameManager.instance.CheckLevelIsClear("D" + 200)) {
            lv4Num.transform.parent.GetComponent<Image>().sprite = UnClearSprite;
        }
        if (!GameManager.instance.CheckLevelIsClear("E" + 200)) {
            lv5Num.transform.parent.GetComponent<Image>().sprite = UnClearSprite;
        }
        if (!GameManager.instance.IsUnlockMaster() && GameManager.instance.masterNum <= 0) {
            lv4Lock.SetActive(true);
            lv5Lock.SetActive(true);
        }else {
            lv4Lock.SetActive(false);
            lv5Lock.SetActive(false);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDifficult(int num) {
        if (num >= 7 && GameManager.instance.masterNum <= 0 && !GameManager.instance.IsUnlockMaster()) {
            Panel.Open("SubPanel");
            return;
        }
        GameManager.instance.currentLevel = num;
        Panel.Open("LevelSelectPanel");
    }
}
