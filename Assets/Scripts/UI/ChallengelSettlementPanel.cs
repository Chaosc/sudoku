﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengelSettlementPanel : Panel {

    public Text currentTime;
    public Text BestTime;

	// Use this for initialization
	void Start () {
        BestTime.text = Localization.GetMultilineText("BestTime") + "    " + GameController.instance.CountTime((long)GameManager.instance.bestScore);
        currentTime.text = Localization.GetMultilineText("CurrentTime") + "    " + GameController.instance.CountTime((long)GameManager.instance.score);
        EzPrefs.SetBool(DateTime.Today.ToString(),true);

        GameManager.instance.AddChanlengeNum();
        if (GameManager.instance.challengeNum % 3 == 0) {
            GameManager.instance.GainMasterNum();
            Panel.Open("MasterQuanPanel");
        }
        if (GameManager.instance.challengeNum % 5 == 0) {
            GameManager.instance.GainHintNum(5);
            Panel.Open("HintQuanPanel");
        }
        GameManager.instance.StopMusic();
        GameManager.instance.PlaySound("win");


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDestroy() {
        base.OnClose();
        GameManager.instance.PlayMusic();
    }

    public void BakcHome() {
        GameManager.instance.isGoDiffcultSelect = true;
        GameManager.instance.LoadScene("Home");
    }
}
