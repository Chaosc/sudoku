﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailSettlementPanel : Panel {

	// Use this for initialization
	void Start () {
        GameManager.instance.StopMusic();
        GameManager.instance.PlaySound("lose");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ReStartGame() {
        GameController.instance.OverTime = 9999999;
        GameManager.instance.isChallenge = true;
        GameManager.instance.currentLevel = 1;
        GameManager.instance.currentLittleLevel = 10;
        GameManager.instance.LoadScene("Game");
    }

    public void AddTime() {
        EzAds.ShowRewardedVideo(() => {
            Panel.Close("FailSettlementPanel");
            GameManager.instance.isChallenge = true;
            GameController.instance.OverTime += 20;
        });
    }

    public void Back() {
        GameManager.instance.LoadScene("Home");
    }

    private void OnDestroy() {
        base.OnClose();
        GameManager.instance.PlayMusic();
    }



    
}
