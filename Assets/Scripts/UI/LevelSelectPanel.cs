﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectPanel : Panel {

    public Text Title;
    //public CanvasScaler canvasScaler;
    public RectTransform[] rtfs;

    // Use this for initialization
    void Start() {
        switch (GameManager.instance.currentLevel) {
            case 1:
                Title.text = Localization.GetMultilineText("Diff1");
                break;
            case 3:
                Title.text = Localization.GetMultilineText("Diff2");
                break;
            case 5:
                Title.text = Localization.GetMultilineText("Diff3");
                break;
            case 7:
                Title.text = Localization.GetMultilineText("Diff4");
                break;
            case 9:
                Title.text = Localization.GetMultilineText("Diff5");
                break;
        }
        //canvasScaler = GameObject.Find("Canvas").GetComponent<CanvasScaler>();
        if (Screen.width == 1125 && Screen.height == 2436) {
            foreach (RectTransform rtf in rtfs) {
                rtf.offsetMin = new Vector2(0, 44f);
                rtf.offsetMax = new Vector2(0, -44f);
            }

        }
        //canvasScaler.matchWidthOrHeight = 0;
    }

        // Update is called once per frame
        void Update () {
		
	}

    private void OnDestroy() {
        //canvasScaler.matchWidthOrHeight = 1;
    }

    protected override void OnClose() {
        base.OnClose();
       // canvasScaler.matchWidthOrHeight = 1;
    }

}
