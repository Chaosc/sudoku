﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackConfirmPanel : Panel {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BackHome() {
        GameManager.instance.isGoDiffcultSelect = true;
        GameManager.instance.LoadScene("Home");
    }

    private void OnDestroy() {
        if (GameController.instance != null) {
            GameController.instance.CancelPause();
        }
    }


}
