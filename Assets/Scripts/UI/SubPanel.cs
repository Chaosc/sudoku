﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubPanel : Panel {
    public Text subText;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ClickSub() {
#if UNITY_IPHONE
        GameManager.instance.PurchaseProduct(0, (product) => {
            GameManager.instance.UnlockMaster();
            GameManager.instance.GainHintNum(35);
            GameManager.instance.noAds = true;
            Panel.Close("SubPanel");
        });
#endif
        GameManager.instance.noAds = true;
    }

}
