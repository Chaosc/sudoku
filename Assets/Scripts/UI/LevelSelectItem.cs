﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectItem : ScrollListItem {
    public Text Num;
    public Sprite UnCompleteSprite;
    public Sprite CompleteSprite;

    string levelPanel;
    bool canClick = false;
    // Use this for initialization
    void Start() {
        Num.text = name;

        levelPanel = "A";
        switch (GameManager.instance.currentLevel) {
            case 1:
                levelPanel = "A";
                break;
            case 3:
                levelPanel = "B";
                break;
            case 5:
                levelPanel = "C";
                break;
            case 7:
                levelPanel = "D";
                break;
            case 9:
                levelPanel = "E";
                break;
        }

        //if (GameManager.instance.CheckLevelIsClear(levelPanel + (index + 1))){
        //    GetComponent<Image>().sprite = CompleteSprite;
        //}else {
        //    GetComponent<Image>().sprite = UnCompleteSprite;
        //}
    
        if (index == 0 || (index != 0 && GameManager.instance.CheckLevelIsClear(levelPanel + (index)))) {
            GetComponent<Image>().sprite = CompleteSprite;
            canClick = true;
        } else {
            GetComponent<Image>().sprite = UnCompleteSprite;
            canClick = false;
        }
        
    }
	// Update is called once per frame
	void Update () {
		
	}

    public void OpenGame() {
        if (GameManager.instance.CheckLevelIsClear(levelPanel + (index + 1))) {
            //string path = Application.dataPath + "/Resources/Level/Sava/" + levelPanel + "/save" + (index + 1) + ".txt";
            string path = Application.persistentDataPath + "/Resources/Level/Sava/" + "/save" + ".txt";
            if (File.Exists(path)) {
                File.Delete(path);
                //GameManager.instance.LevelClearFalse(levelPanel+ (index + 1));
            }
            GameManager.instance.SetSavaLevel("");
            GameManager.instance.SetSavaLittleLevel("");
        }
        if (canClick) {
            if ("".Equals(GameManager.instance.CheckSavaLevel())) {
                GameManager.instance.currentLittleLevel = index + 1;
                GameManager.instance.LoadScene("Game");
            } else {
                Panel.Open("ContinueLevelPanel");

            }
        }

    }
}
