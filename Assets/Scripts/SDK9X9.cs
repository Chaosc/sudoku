﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RepealData {
    public GameObject tt;
    public string x;

    public RepealData(GameObject tt, string x) {
        this.tt = tt;
        this.x = x;
    }
}


public class SDK9X9 : MonoBehaviour {
    [SerializeField]
    private GameObject parentSDK;
    [SerializeField]
    private GameObject selectBack;

    private GameObject btnClick;

    private List<GameObject> preSDKlist = new List<GameObject>();

    private List<Button> btnList = new List<Button>();

    private List<RepealData> RepealList = new List<RepealData>();

    private List<GameObject> playerBtnList = new List<GameObject>();
    [SerializeField]
    private Text timeText;

    private float tTimeFloat = 0;
    private bool isGameBool = false;

    private int xyInt_Max = 9;
    private int xyInt_Min = 3;
    private int[,] sdkIntList;

    private int level = 0;
    [SerializeField]
    private Text levleBtnText;

    private bool randomBool = false;

    [SerializeField]
    private Text winText;

    public bool isPencil = false;
    public CanvasScaler canvas9;
    public GridLayoutGroup gridLayout;

    // Use this for initialization
    void Start () {

        canvas9.referenceResolution = new Vector2(Screen.width, Screen.height);
        gridLayout.cellSize = new Vector2((int)((Screen.width-4f-2f*8f)/9f), (int)((Screen.width - 4f - 2f * 8f) / 9f));
        gridLayout.padding = new RectOffset(0, 0, (int)(140f * (Screen.height / 1136f)), 0);
        sdkIntList = new int[xyInt_Max, xyInt_Max];
        sdkIntListB = new int[xyInt_Max, xyInt_Max];
        tTimeFloat = float.Parse(Time.time.ToString());


        bool isPad = false;

#if UNITY_IPHONE
        Debug.Log(UnityEngine.iOS.Device.generation);
        string iP_genneration = UnityEngine.iOS.Device.generation.ToString();
        //The second Mothod: 
        //string iP_genneration=SystemInfo.deviceModel.ToString();
 
        if (iP_genneration.Substring(0, 3) == "iPa")
        {
            isPad = true;
        }
#endif

        if (isPad) {
            gridLayout.cellSize = new Vector2((int)((Screen.width - 4f - 2f * 8f) / 9f/1.7f), (int)((Screen.width - 4f - 2f * 8f) / 9f/1.7f));
            gridLayout.padding = new RectOffset(0, 0, (int)(140f * (Screen.height / 1136f) * 1.5f), 0);
        }

        for (int x = 0;x < 81;x++)
		{
            Object ins = Resources.Load("ButtonSDK", typeof(GameObject));
            GameObject insx = Instantiate(ins, parentSDK.transform) as GameObject;

            insx.transform.Find("Text").GetComponent<Text>().fontSize = (int)(gridLayout.cellSize.x / 68f * 26f);

            if (x % 9 == 3) {
                insx.transform.Find("GameObject").GetComponent<RectTransform>().offsetMin = new Vector2(2, 0);
            }
            if (x % 9 == 6) {
                insx.transform.Find("GameObject").GetComponent<RectTransform>().offsetMin = new Vector2(2, 0);
            }

            if (x / 9 == 3) {
                insx.transform.Find("GameObject").GetComponent<RectTransform>().offsetMax = new Vector2(0, -2);
            }
            if (x / 9 == 6) {
                insx.transform.Find("GameObject").GetComponent<RectTransform>().offsetMax = new Vector2(0, -2);
            }

            insx.name = x.ToString();
            preSDKlist.Add(insx);
            GameItem gameItem = insx.AddComponent<GameItem>();
            gameItem.index = x;
            btnList.Add(insx.GetComponent<Button>());                                                    
            btnList[x].onClick.AddListener(
                delegate ()
                {
                    OnButtonClick(insx);
                }
            );
        }
        if (GameManager.instance.isChallenge) {
            this.level = GameManager.instance.currentLevel;
            randomGo(2);
            randomGo(0);
        } else {
            InitLevel();
        }
        //level = 1;
        //for (int i = 0; i < 200; i++) {
        //    sdkIntList = new int[xyInt_Max, xyInt_Max];
        //    iii++;
        //    randomGo(2);
        //    GetLevel("A");
        //}
        //iii = 0;
        //level = 3;
        //for (int i = 0; i < 200; i++) {
        //    sdkIntList = new int[xyInt_Max, xyInt_Max];
        //    iii++;
        //    randomGo(2);
        //    GetLevel("B");
        //}
        //iii = 0;
        //level = 5;
        //for (int i = 0; i < 200; i++) {
        //    sdkIntList = new int[xyInt_Max, xyInt_Max];
        //    iii++;
        //    randomGo(2);
        //    GetLevel("C");
        //}
        //iii = 0;
        //level = 7;
        //for (int i = 0; i < 200; i++) {
        //    sdkIntList = new int[xyInt_Max, xyInt_Max];
        //    iii++;
        //    randomGo(2);
        //    GetLevel("D");
        //}
        //iii = 0;
        //level = 10;
        //for (int i = 0; i < 200; i++) {
        //    sdkIntList = new int[xyInt_Max, xyInt_Max];
        //    iii++;
        //    randomGo(2);
        //    GetLevel("E");
        //}

    }

    private int iii = 0;
    string levelPanel;
    private int[,] sdkIntListB;
    public void InitLevel() {
        this.level = GameManager.instance.currentLevel;
        //randomGo(2);
        levelPanel  = "A";
        switch (GameManager.instance.currentLevel) {
            case 1:
                levelPanel = "A";
                break;
            case 3:
                levelPanel = "B";
                break;
            case 5:
                levelPanel = "C";
                break;
            case 7:
                levelPanel = "D";
                break;
            case 9:
                levelPanel = "E";
                break;
        }

        TextAsset textsA = Resources.Load<TextAsset>("Level/"+ levelPanel + "/save"+GameManager.instance.currentLittleLevel);
        // TextAsset textsB = Resources.Load<TextAsset>("Level/Sava/" + levelPanel + "/save" + GameManager.instance.currentLittleLevel);
        //TextAsset textsB = Resources.Load<TextAsset>("Level/Sava/" + "save");
        string path = Application.persistentDataPath + "/Resources/Level/Sava/" + "/save" + ".txt";
        string textsB = "";
        if (File.Exists(path)) {
            StreamReader sr = File.OpenText(path);
            textsB = sr.ReadToEnd();
            sr.Close();
        }
        int temp = 0;
        if (File.Exists(path)) {
            List<object> leveldateB = (List<object>)MiniJSON.Json.Deserialize(textsB);
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    sdkIntListB[i, j] = System.Convert.ToInt32(leveldateB[temp].ToString());
                    //if(sdkIntList[i, j] > 10) {
                    //    sdkIntList[i, j] = sdkIntList[i, j] - 10;
                    //}
                    temp++;
                }
            }
        }
        List<object> leveldate =  (List<object>)MiniJSON.Json.Deserialize(textsA.ToString());
        Debug.LogError(textsA);
        
        temp = 0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                sdkIntList[i, j] = System.Convert.ToInt32(leveldate[temp].ToString());
                //if(sdkIntList[i, j] > 10) {
                //    sdkIntList[i, j] = sdkIntList[i, j] - 10;
                //}
                temp++;
            }
        }
        if("".Equals(GameManager.instance.CheckSavaLevel())) {
            CreateGame();
        }else {
            CreateSavedGame();
        }

        
        randomGo(0);
        //GameObject tt = GameObject.Find("Canvas/Back/ImageBack/Image40X9/" + step + "/Text");
    }


    // Update is called once per frame

    void Update () {
		if (Input.GetMouseButtonDown(1))
		{
            selectBack.SetActive(false);
            btnClick = null;
        }
        if (isGameBool)
        {
            timeText.text = (float.Parse(Time.time.ToString()) - tTimeFloat).ToString();
        }

    }

    // 待监听的事件
    void OnButtonClick(GameObject objectx)
    {
        if (!isGameBool)
        {
            return;
        }
        btnClick = objectx;
    
        int index =  btnClick.GetComponent<GameItem>().index;
        int temp = index % 9;
        for(int i = 0; i <= 80; i++) {
            GameObject tempGo = GameObject.Find("Canvas9/ImageBack/Image40X9/" + i);
            tempGo.transform.Find("GameObject").gameObject.GetComponent<Image>().color = Color.white;
            tempGo.transform.Find("GameObject").gameObject.GetComponent<Image>().transform.localScale = new Vector3(1f, 1f, 1f);
            tempGo.transform.Find("Shadow").gameObject.SetActive(false);
            GameObject ttGo =  tempGo.transform.Find("Text").gameObject;
            ttGo.GetComponent<Text>().color = new Color(1f / 255f, 0, 34f / 255f);
            if(playerBtnList.Contains(ttGo)) {
                ttGo.GetComponent<Text>().color = new Color(229f / 255f, 162f/255f, 0);
            }
          
            Destroy(tempGo.GetComponent<Canvas>());
            if (i % 9 == temp) {
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + i);
                tt.gameObject.transform.Find("GameObject").GetComponent<Image>().color = new Color(229f/255f, 248f/255f, 255f/255f);
            }
            if (i / 9 == index/9) {
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + i);
                tt.gameObject.transform.Find("GameObject").GetComponent<Image>().color = new Color(229f / 255f, 248f / 255f, 255f / 255f);
            }
            string tempGOStr =  tempGo.transform.Find("Text").GetComponent<Text>().text;
            if (btnClick.transform.Find("Text").GetComponent<Text>().text.Equals(tempGOStr) && !"".Equals(tempGOStr)) {
                //tempGo.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
                tempGo.transform.Find("Text").GetComponent<Text>().color = new Color(0,131f/255f,232f/255f);
            }
        }
        Image selectImage = btnClick.transform.Find("GameObject").gameObject.GetComponent<Image>();
        selectImage.transform.parent.Find("Text").GetComponent<Text>().color =Color.white;
        selectImage.color = new Color(65f / 255f, 224f / 255f, 253f / 255f);
        selectImage.transform.parent.Find("Shadow").gameObject.SetActive(true);
        selectImage.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
        Canvas canvas =  btnClick.AddComponent<Canvas>();
        canvas.overrideSorting = true;
        canvas.sortingOrder = 2;
    } 

    public void selectInt(int x)
    {

        if (btnClick != null) {
            GameManager.instance.PlaySound("block");
            GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + btnClick.name + "/Text");
            if (tt.transform.parent.GetComponent<GameItem>().CanChange) {
                if (isPencil) {
                    SetPencilNum(x);
                } else {
                    tt.transform.parent.GetComponent<GameItem>().HindPencel();  
                    if ("".Equals(tt.GetComponent<Text>().text)) {
                        RepealList.Add(new RepealData(tt, ""));
                    }
                    tt.GetComponent<Text>().text = x.ToString();
                    int index = tt.transform.parent.GetComponent<GameItem>().index;
                    //if (x != sdkIntList[index / 9, index % 9]) {
                    //    tt.GetComponent<Text>().color = Color.red;
                    //} else {
                    //    tt.GetComponent<Text>().color = Color.white;
                    //}
                    tt.GetComponent<Text>().color = Color.white;
                    RepealList.Add(new RepealData(tt,x.ToString()));
                    Debug.LogError(CheckNineResult(  index % 9 / 3, index / 9 / 3) +"checkNineCells" );
                    Debug.LogError(checkColResoult(index % 9) + "checkColResoult");
                    Debug.LogError(checkRowResult(index / 9) + "checkRowResult");
                    
                }
            }
        }
        
        randomGo(1);

        SavePlayerLevel();

    }


    private void SavePlayerLevel() {
        if(!GameManager.instance.isChallenge) {
            int ranMax = 12;
            int l_int = level + 1;
            int step = 0;
            for (int i = 0; i < xyInt_Max; i++) {
                for (int j = 0; j < xyInt_Max; j++) {
                    int rn = Random.Range(0, ranMax);
                    //Debug.Log(rn);
                    GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + step + "/Text");
                    if ("".Equals(tt.GetComponent<Text>().text)) {
                        sdkIntListB[i, j] = 0;
                    }else {
                        sdkIntListB[i, j] = System.Convert.ToInt32(tt.GetComponent<Text>().text);
                    }
                    step++;
                }
            }
            string sss = MiniJSON.Json.Serialize(sdkIntListB);
            SaveP(GameManager.instance.currentLittleLevel + "", levelPanel, sss);
        }


    }

    private void SetPencilNum(int x) {
        if (btnClick != null) {
            GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + btnClick.name + "/Text");
            tt.transform.parent.GetComponent<GameItem>().ShowPencil(x);
        }
    }

    public void randomGo(int x)
    {
        switch(x)
        {
            case 0://开始
                //if (!isGameBool && randomBool)
                //{
                    tTimeFloat = float.Parse(Time.time.ToString());
                    isGameBool = true;
                    winText.text = "";

                //}
                break;
            case 1://完成
                if (isGameBool)
                {
               
                    int st = 0;
                    bool boolWin = true;
                    bool complete = true;
                    for (int i = 0;i < 9;i++)
                    {
                        for (int j = 0;j < 9;j++)
                        {
                          
                            GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + st + "/Text");
                         
                            if ("".Equals(tt.GetComponent<Text>().text)) {
                                complete = false;
                            }
                            if (tt.GetComponent<Text>().text == sdkIntList[i, j].ToString())
                            {


                            } 
                            else
                            {
                                boolWin = false;
                            }
                            st++;

                           
                        }
                    }
                    st = 0;
                    float delayAnima = 0f;
                    if (boolWin)
                    {
                        for (int i = 0; i < 9; i++) {
                            for (int j = 0; j < 9; j++) {
                                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + st);
                                Tweener tn =  tt.transform.DOScale(new Vector3(1.4f,1.4f,1.4f),0.2f);
                                tn.SetDelay<Tweener>(delayAnima);
                                st++;
                                EzTiming.CallDelayed(delayAnima + 0.2f, () => {
                                    if (tt != null) {
                                        tt.transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
                                    }
                                });
                                delayAnima += 0.1f;
                            }
                        }

                        GameController.instance.ShowGameOverImage();

                        EzTiming.CallDelayed(2.5f, () => {
                            GameController.instance.HideGameOverImage();
                            isGameBool = false;
                            randomBool = false;
                            winText.text = "恭喜你，数独成功完成！";
                            if(!GameManager.instance.CheckLevelIsClear(levelPanel + GameManager.instance.currentLittleLevel)) {
                                switch (levelPanel) {
                                    case "A":
                                        GameManager.instance.GainDif1Num();
                                        break;
                                    case "B":
                                        GameManager.instance.GainDif2Num();
                                        break;
                                    case "C":
                                        GameManager.instance.GainDif3Num();
                                        break;
                                    case "D":
                                        GameManager.instance.GainDif4Num();
                                        break;
                                    case "E":
                                        GameManager.instance.GainDif5Num();
                                        break;
                                }
                            }
                            GameManager.instance.LevelClear(levelPanel + GameManager.instance.currentLittleLevel);
                            //if (!GameManager.instance.isChallenge) {
                            GameController.instance.GameOver();
                            //}else {
                            //    Panel.Open("ChallengelSettlementPanel");
                            //}
                          
                            string path = Application.persistentDataPath + "/Resources/Level/Sava/" + "/save" + ".txt";
                            if (File.Exists(path)) {
                                File.Delete(path);
                                GameManager.instance.SetSavaLevel("");
                                GameManager.instance.SetSavaLittleLevel("");
                            }

                        });
                       
                    } 
                    else
                    {
                        if (complete) {
                            Toast.Show("填写出现错误，请检查");
                        }

                    }
                }
                break;
            case 2://生成实例
                //if (isGameBool)
                //{
                //    return;
                //}
                winText.text = "";
                int ran = 1;
                int step = 0;
                for (int i = 0;i< xyInt_Max;i++)
                {
                    for (int j = 0;j < xyInt_Max;j++)
                    {
                        int p = int.Parse(Mathf.Ceil(i / 3).ToString());
                        int q = int.Parse(Mathf.Ceil(j / 3).ToString());

                        for (int k = 0;k < xyInt_Max;k++)
                        {
                            if (checkRow(ran,i) && checkCol(ran,j) && checkNineCells(ran,p,q))
                            {
                                sdkIntList[i, j] = ran;
                                //初始填充九宫格
                                //GameObject tt = GameObject.Find("Canvas/Back/ImageBack/Image40X9/" + step + "/Text");
                                //tt.GetComponent<Text>().text = ran.ToString();
                                break;
                            }
                            else
                            {
                                ran = ran % xyInt_Max + 1;
                            }
                        }
                        step++;
                    }
                    ran = ran % xyInt_Max + 1;
                }

                upset();//随机打乱
                randomBool = true;
                break;

            case 3://难度1~10
                string name = "";
                if (level < 10)
                {
                    level++;
                }
                else level = 1;
                switch (level)
                {
                    case 1:
                        name = "难度二";
                        break;
                    case 2:
                        name = "难度三";
                        break;
                    case 3:
                        name = "难度四";
                        break;
                    case 4:
                        name = "难度五";
                        break;
                    case 5:
                        name = "难度六";
                        break;
                    case 6:
                        name = "难度七";
                        break;
                    case 7:
                        name = "难度八";
                        break;
                    case 8:
                        name = "难度九";
                        break;
                    case 9:
                        name = "难度十";
                        break;
                    case 10:
                        name = "难度一";
                        break;
                }

                levleBtnText.text = name;

                
                break;
        }
    }

    //检查所在行
    bool checkRow(int n,int row)
    {
        bool bl = true;
        for (int x = 0;x < xyInt_Max;x++)
        {
            if (sdkIntList[row,x] == n)
            {
                bl = false;
                
                break;
            }
        }
        return bl;
    }
    //检查所在列
    bool checkCol(int n, int col)
    {
        bool bl = true;
        for (int x = 0; x < xyInt_Max; x++)
        {
            if (sdkIntList[x, col] == n)
            {
                bl = false;
                break;
            }   
        }
        return bl;
    }



    //检查所在小9宫格
    bool checkNineCells(int n, int x,int y)
    {
        bool result = true;
        int sx = x * 3, sy = y * 3;
        
        for (int i = sx; i < sx + 3; i++)
        {
            for (int j = sy; j < sy + 3; j++)
            {
               if (sdkIntList[i,j] == n)
                {
                   result = false;
                   break;
                }
            }
           if (!result) break;
       }
      return result;
    }



    bool CheckNineResult(int x,int y) {
        bool result = true;
        int sx = x * 3, sy = y * 3;
        for (int i = sx; i < sx + 3; i++) {
            for (int j = sy; j < sy + 3; j++) {
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + (j*9 +i)   + "/Text");
                Debug.LogError(tt.GetComponent<Text>().text);
                if(!tt.GetComponent<Text>().text.Equals(sdkIntList[j, i].ToString())) {
                    result = false;
                    break;
                }
            }
            if (!result) break;
        }

        if(result) {
            for (int i = sx; i < sx + 3; i++) {
                for (int j = sy; j < sy + 3; j++) {
                    GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + (j * 9 + i) + "/Text");
                    ShowCompleteAnima(tt);
                }
               
            }
        }
        delayAnima = 0f;
        return result;
    }
    float delayAnima = 0f;
    private void ShowCompleteAnima(GameObject tt) {
      
        Tweener tn = tt.transform.parent.DOScale(new Vector3(1.4f, 1.4f, 1.4f), 0.2f);
        tn.SetDelay<Tweener>(delayAnima);
        EzTiming.CallDelayed(delayAnima + 0.2f, () => {
            if (tt != null) {
                tt.transform.parent.DOScale(new Vector3(1f, 1f, 1f), 0.2f);
            }
        });
        delayAnima += 0.1f;
    }


    //检查所在列
    bool checkColResoult(int col) {
        bool bl = true;
        for (int x = 0; x < xyInt_Max; x++) {
            GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + (x * 9 + col) + "/Text");

            if (!tt.GetComponent<Text>().text.Equals(sdkIntList[x, col].ToString())) {
                bl = false;
                break;
            }
        }

        if(bl) {
            for (int x = 0; x < xyInt_Max; x++) {
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + (x * 9 + col) + "/Text");
                ShowCompleteAnima(tt);
            }
        }
        return bl;
    }

    //检查所在行
    bool checkRowResult(int row) {
        bool bl = true;
        for (int x = 0; x < xyInt_Max; x++) {
            GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + (row * 9 + x) + "/Text");
            if (!tt.GetComponent<Text>().text.Equals(sdkIntList[row, x].ToString())) {
                bl = false;
                break;
            }
        }
        if (bl) {
            for (int x = 0; x < xyInt_Max; x++) {
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + (row * 9 + x) + "/Text");
                ShowCompleteAnima(tt);
                if(btnClick) {

                }
            }
        }
        return bl;
    }

    //随机打乱顺序
    void upset()
    {
        //按行交换
        for (int i = 0; i < xyInt_Max; i++)
        {
            int n = int.Parse((i / 3).ToString());
            int xxx = (n % 3) * 3 + 2;
            int p = Random.Range(n % 3 * 3, xxx);

            for (int j = 0; j < xyInt_Max; j++)
            {
                int tmp = sdkIntList[i,j];
                sdkIntList[i, j] = sdkIntList[p, j];
                sdkIntList[p, j] = tmp;
             }
         }
         //按列交换
         for (int i = 0; i < xyInt_Max; i++)
        {
            int n = int.Parse((i / 3).ToString());
            int xxx = (n % 3) * 3 + 2;
            int p = Random.Range(n % 3 * 3, xxx);
            for (int j = 0; j < xyInt_Max; j++)
            {
                int tmp = sdkIntList[j,i];
                sdkIntList[j, i] = sdkIntList[j, p];
                sdkIntList[j, p] = tmp;
             }
         }
        //Debug.LogError(i + "i" + j + "j" + ran + "ran");

        //Debug.Log(level);

        //CreateGame();
        int ranMax = 12;
        int l_int = level + 1;
        int step = 0;
        for (int i = 0; i < xyInt_Max; i++) {
            for (int j = 0; j < xyInt_Max; j++) {
                int rn = Random.Range(0, ranMax);
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + step + "/Text");
                //if (sdkIntList[i, j] < 10) {
                 if (rn >= l_int) { 
                    tt.GetComponent<Text>().text = sdkIntList[i, j].ToString();
                    tt.transform.parent.gameObject.GetComponent<GameItem>().CanChange = false;
                } else {
                    tt.GetComponent<Text>().text = "";
                    tt.transform.parent.gameObject.GetComponent<GameItem>().CanChange = true;
                    playerBtnList.Add(tt);
                }
                step++;
            }
        }
    }


    private void CreateGame() {
        int ranMax = 12;
        int l_int = level + 1;
        int step = 0;
        for (int i = 0; i < xyInt_Max; i++) {
            for (int j = 0; j < xyInt_Max; j++) {
                int rn = Random.Range(0, ranMax);
                //Debug.Log(rn);
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + step + "/Text");
                if (sdkIntList[i, j] < 10) {
               // if (rn >= l_int) { 
                    tt.GetComponent<Text>().text = sdkIntList[i, j].ToString();
                    tt.transform.parent.gameObject.GetComponent<GameItem>().CanChange = false;
                } else {
                    tt.GetComponent<Text>().text = "";
                    sdkIntList[i, j] -= 10;
                    tt.transform.parent.gameObject.GetComponent<GameItem>().CanChange = true;
                    playerBtnList.Add(tt);
                }
                step++;
            }
        }
    }



    private void CreateSavedGame() {
        int ranMax = 12;
        int l_int = level + 1;
        int step = 0;
        for (int i = 0; i < xyInt_Max; i++) {
            for (int j = 0; j < xyInt_Max; j++) {
                int rn = Random.Range(0, ranMax);
                //Debug.Log(rn);
                GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + step + "/Text");
                if (sdkIntListB[i, j] > 0) {
                    // if (rn >= l_int) { 
                    tt.GetComponent<Text>().text = sdkIntListB[i, j].ToString();
                } else {
                    tt.GetComponent<Text>().text = "";
                    sdkIntListB[i, j] = sdkIntListB[i, j] - 10;
                }
                if (sdkIntList[i, j] < 10) {
                    tt.transform.parent.gameObject.GetComponent<GameItem>().CanChange = false;
                } else {    
                    sdkIntList[i, j] -= 10;
                    tt.transform.parent.gameObject.GetComponent<GameItem>().CanChange = true;
                    playerBtnList.Add(tt);
                    tt.GetComponent<Text>().color = new Color(229f / 255f, 162f / 255f, 0);
                }
                step++;
            }
        }
    }


    private void GetLevel(string plane) {
       string sss =  MiniJSON.Json.Serialize(sdkIntList);
        SaveS(iii+"", plane, sss);
       Debug.LogError(sss);
    }
    public void SaveS(string levelNum, string levelPlan,string LevelDataStr) {

        if (!Directory.Exists(Application.dataPath + "/Resources/Level/" + levelPlan)) {
            Directory.CreateDirectory(Application.dataPath + "/Resources/Level/" + levelPlan);
        }
        FileStream fs = new FileStream(Application.dataPath + "/Resources/Level/" + levelPlan + "/save" + levelNum + ".txt", FileMode.Create);
        byte[] bytes = new UTF8Encoding().GetBytes(LevelDataStr.ToString());
        fs.Write(bytes, 0, bytes.Length);
        fs.Close();
    }

    public void SaveP(string levelNum, string levelPlan, string LevelDataStr) {
        if (!Directory.Exists(Application.persistentDataPath + "/Resources/Level/Sava/" + levelPlan)) {
            Directory.CreateDirectory(Application.persistentDataPath + "/Resources/Level/Sava/" + levelPlan);
        }
        //FileStream fs = new FileStream(Application.dataPath + "/Resources/Level/Sava/" + levelPlan + "/save" + levelNum + ".txt", FileMode.Create);
        FileStream fs = new FileStream(Application.persistentDataPath + "/Resources/Level/Sava/" + "/save" + ".txt", FileMode.OpenOrCreate);
        byte[] bytes = new UTF8Encoding().GetBytes(LevelDataStr.ToString());
        fs.Write(bytes, 0, bytes.Length);
        fs.Close();

        GameManager.instance.SetSavaLevel(levelPlan);
        GameManager.instance.SetSavaLittleLevel(levelNum);

    }



    public void Repeal() {
        if(RepealList.Count > 1) {
            RepealList.RemoveAt(RepealList.Count - 1);
            RepealData rd =  RepealList[RepealList.Count - 1];
            rd.tt.GetComponent<Text>().text = rd.x.ToString();
            RepealList.RemoveAt(RepealList.Count - 1);
        }
    }

    public void Hint() {
        if(btnClick != null) {
            GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + btnClick.name + "/Text");
            int index = tt.transform.parent.GetComponent<GameItem>().index;
            tt.GetComponent<Text>().text = sdkIntList[index / 9, index % 9].ToString();
            tt.GetComponent<Text>().color = Color.white;
            SavePlayerLevel();
        }
    }
    public void Rm() {
        if (btnClick != null) {
            GameObject tt = GameObject.Find("Canvas9/ImageBack/Image40X9/" + btnClick.name + "/Text");
            if (tt.transform.parent.gameObject.GetComponent<GameItem>().CanChange) {
                tt.GetComponent<Text>().text = "";
                SavePlayerLevel();
            }

        }
    }

    public void Pencil() {
        isPencil = !isPencil;
    }

}
