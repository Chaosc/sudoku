﻿using UnityEngine;
using System.Collections.Generic;

public class ActiveByLanguage : MonoBehaviour {

    public string[] languages;

    // Use this for initialization
    void Start() {
        HashSet<string> langSet = new HashSet<string>();
        foreach (var lan in languages) {
            langSet.Add(lan.ToUpper());
        }
        gameObject.SetActive(langSet.Contains(Localization.GetLanguage()));
	}
	
}
