﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LocalizedText : MonoBehaviour {

    public string key;

    public LocalFont[] fonts;

    private Dictionary<string, Font> localFonts;
    private Font localFont {
        get {
            if (localFonts != null) {
                string language = Localization.GetLanguage();
                if (localFonts.ContainsKey(language)) {
                    return localFonts[language];
                }
            }
            return GameManager.instance.localFont;
        }
    }

    void Awake() {
        if (fonts.Length > 0) {
            localFonts = new Dictionary<string, Font>();
            foreach (LocalFont font in fonts) {
                localFonts.Add(font.language.ToUpper(), font.font);
            }
        }
    }

    // Use this for initialization
    void Start() {
        Text text = GetComponent<Text>();
        text.text = Localization.GetMultilineText(key);
        Font font = localFont;
        if (font != null) {
            text.font = font;
        }
        ContentSizeFitter fitter = GetComponent<ContentSizeFitter>();
        if (fitter != null) {
            fitter.SetLayoutHorizontal();
        }
    }

}
