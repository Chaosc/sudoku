﻿using UnityEngine;

[System.Serializable]
public struct LocalSprite {
    public string language;
    public Sprite sprite;
}
