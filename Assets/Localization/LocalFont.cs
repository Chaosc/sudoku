﻿using UnityEngine;

[System.Serializable]
public struct LocalFont {
    public string language;
    public Font font;
}
