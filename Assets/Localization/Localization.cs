﻿using UnityEngine;
using System.Collections.Generic;

public class Localization {

    public const string LANGUAGE_CHINESE = "CN";
    public const string LANGUAGE_CHINESE_TRADITIONAL = "CT";
	public const string LANGUAGE_ENGLISH = "EN";
    public const string LANGUAGE_FRENCH = "FR";
    public const string LANGUAGE_GERMAN = "GE";
    public const string LANGUAGE_ITALY = "IT";
	public const string LANGUAGE_JAPANESE = "JP";
    public const string LANGUAGE_KOREA = "KR";
    public const string LANGUAGE_RUSSIA = "RU";
    public const string LANGUAGE_SPANISH = "SP";
    public const string LANGUAGE_PORTUGUESE = "PR";
    public const string LANGUAGE_INDONESIAN = "ID";

    private static readonly string INPUT_CSV_FILE_PATH = Application.dataPath + "/../Resources/localization.csv";
    private static readonly string OUTPUT_TXT_FILE_PATH = Application.dataPath + "/Localization/Resources/Localization/";

    private Dictionary<string, string> textData = new Dictionary<string, string>();

    private static Localization mInstance;

    private Localization() {
        ReadData();
    }

    public static string GetLanguage() {
        return GetLanguage(GameManager.instance.testLanguage != SystemLanguage.Unknown
            ? GameManager.instance.testLanguage
            : Application.systemLanguage);
    }

    private static string GetLanguage(SystemLanguage language) {
        switch (language) {
            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseSimplified:
                return LANGUAGE_CHINESE;
            case SystemLanguage.ChineseTraditional:
                return LANGUAGE_CHINESE_TRADITIONAL;
        }
        return LANGUAGE_ENGLISH;
    }

    private static string GetLanguageFive(SystemLanguage language) {
        switch (language) {
            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseSimplified:
                return LANGUAGE_CHINESE;
            case SystemLanguage.ChineseTraditional:
                return LANGUAGE_CHINESE_TRADITIONAL;
            case SystemLanguage.Japanese:
                return LANGUAGE_JAPANESE;
            case SystemLanguage.Korean:
                return LANGUAGE_KOREA;
        }
        return LANGUAGE_ENGLISH;
    }

    private static string GetLanguageNine(SystemLanguage language) {
        switch (language) {
            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseSimplified:
                return LANGUAGE_CHINESE;
            case SystemLanguage.ChineseTraditional:
                return LANGUAGE_CHINESE_TRADITIONAL;
            case SystemLanguage.French:
                return LANGUAGE_FRENCH;
            case SystemLanguage.German:
                return LANGUAGE_GERMAN;
            case SystemLanguage.Russian:
                return LANGUAGE_RUSSIA;
            case SystemLanguage.Spanish:
                return LANGUAGE_SPANISH;
            case SystemLanguage.Portuguese:
                return LANGUAGE_PORTUGUESE;
            case SystemLanguage.Indonesian:
                return LANGUAGE_INDONESIAN;
        }
        return LANGUAGE_ENGLISH;
    }

    private static string GetLanguageAll(SystemLanguage language) {
        switch (language) {
            case SystemLanguage.Afrikaans:
            case SystemLanguage.Arabic:
            case SystemLanguage.Basque:
            case SystemLanguage.Belarusian:
            case SystemLanguage.Bulgarian:
            case SystemLanguage.Catalan:
                return LANGUAGE_ENGLISH;
            case SystemLanguage.Chinese:
            case SystemLanguage.ChineseTraditional:
            case SystemLanguage.ChineseSimplified:
                return LANGUAGE_CHINESE;
            case SystemLanguage.Czech:
            case SystemLanguage.Danish:
            case SystemLanguage.Dutch:
            case SystemLanguage.English:
            case SystemLanguage.Estonian:
            case SystemLanguage.Faroese:
            case SystemLanguage.Finnish:
                return LANGUAGE_ENGLISH;
            case SystemLanguage.French:
                return LANGUAGE_FRENCH;
            case SystemLanguage.German:
                return LANGUAGE_GERMAN;
            case SystemLanguage.Greek:
            case SystemLanguage.Hebrew:
            case SystemLanguage.Icelandic:
            case SystemLanguage.Indonesian:
                return LANGUAGE_ENGLISH;
            case SystemLanguage.Italian:
                return LANGUAGE_ITALY;
            case SystemLanguage.Japanese:
                return LANGUAGE_JAPANESE;
            case SystemLanguage.Korean:
                return LANGUAGE_KOREA;
            case SystemLanguage.Latvian:
            case SystemLanguage.Lithuanian:
            case SystemLanguage.Norwegian:
            case SystemLanguage.Polish:
            case SystemLanguage.Portuguese:
            case SystemLanguage.Romanian:
                return LANGUAGE_ENGLISH;
            case SystemLanguage.Russian:
                return LANGUAGE_RUSSIA;
            case SystemLanguage.SerboCroatian:
            case SystemLanguage.Slovak:
            case SystemLanguage.Slovenian:
                return LANGUAGE_ENGLISH;
            case SystemLanguage.Spanish:
                return LANGUAGE_SPANISH;
            case SystemLanguage.Swedish:
            case SystemLanguage.Thai:
            case SystemLanguage.Turkish:
            case SystemLanguage.Ukrainian:
            case SystemLanguage.Vietnamese:
            case SystemLanguage.Unknown:
                return LANGUAGE_ENGLISH;
        }
        return LANGUAGE_CHINESE;
    }

    private static string GetWinSavePath(string fileName) {
        return OUTPUT_TXT_FILE_PATH + fileName + ".txt";
    }

    private void ReadData() {
#if UNITY_EDITOR
        // 在Windows平台下读取语言配置文件
        CSVLoader loader = new CSVLoader();
        loader.ReadUTF8File(INPUT_CSV_FILE_PATH);
        // 将配置文件序列化为多个语言类
        int rows = loader.GetRows();
        int cols = loader.GetCols();
        for (int col = 1; col < cols; ++col) {
            LanguageData languageData = new LanguageData();
            // 获取第一行数据(语言类型)
            languageData.type = loader.GetValueAt(col, 0);
            // 遍历生成变量
            languageData.data = new Dictionary<string, string>();
            for (int row = 1; row < rows; ++row) {
				string key = loader.GetValueAt(0, row);
				string value = loader.GetValueAt(col, row);
				if (languageData.data.ContainsKey(key)) {
					Debug.LogWarning("Duplicate key: " + key + " at line #" + (row + 1));
				} else {
					languageData.data.Add(key, value);
				}
            }
            // 将语言对象序列化存档
            SaveHelper.SaveData(GetWinSavePath(languageData.type), languageData);
            if (GetLanguage().Equals(languageData.type)) {
                textData = languageData.data;
            }
        }
#else
        // 读取对应的语言对象
        string lang = GetLanguage();
        TextAsset tempAsset = Resources.Load("Localization/" + lang) as TextAsset;
        Debug.Log("language: " + lang + ", tempAsset=" + tempAsset);
        if (null == tempAsset) {
            tempAsset = Resources.Load("Localization/" + "EN", typeof(TextAsset)) as TextAsset;
        }
        if (null == tempAsset) {
            Debug.LogError("未检测到语言配置文件");
        } else {
            string saveData = tempAsset.text;
            LanguageData languageData = SaveHelper.LoadData(saveData, typeof(LanguageData), false) as LanguageData;
            textData = languageData.data;
        }
#endif
    }

    public static void Init() {
        mInstance = new Localization();
    }

    public static bool HasText(string key) {
        if (null == mInstance) {
            Init();
        }
        return mInstance.textData.ContainsKey(key);
    }

    public static string GetText(string key) {
        if (HasText(key)) {
			return mInstance.textData[key];
        }
        return "[NoDefine]" + key;
    }

    public static string GetMultilineText(string key) {
		return GetText(key).Replace("\\n", "\n").Replace("\\t", "\t");
    }

}