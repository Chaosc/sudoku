﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShareRewardButton : MonoBehaviour {

    // Use this for initialization
    void Start() {
        GetComponent<Button>().onClick.AddListener(() => {
            GameManager.instance.OpenShareReward();
        });
    }

    // Update is called once per frame
    void Update() {

    }
}
