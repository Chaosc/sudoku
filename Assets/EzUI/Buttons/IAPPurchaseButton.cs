﻿#if UNITY_PURCHASING
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class IAPPurchaseButton : MonoBehaviour {

    public int productIndex;

    [System.Serializable]
    public class PurchaseSucceededEvent : UnityEvent<Product> { }
    public PurchaseSucceededEvent onPurchaseSucceeded;

    [System.Serializable]
    public class PurchaseFailededEvent : UnityEvent<Product, PurchaseFailureReason> { }
    public PurchaseFailededEvent onPurchaseFailed;

    private Button button;

    // Use this for initialization
    void Start() {
        button = GetComponent<Button>();
        button.onClick.AddListener(Purchase);
    }

    void Purchase() {
        button.interactable = false;
#if USE_IAP
        GameManager.instance.PurchaseProduct(productIndex, OnPurchaseSucceeded, OnPurchaseFailed);
#endif
    }

    void OnPurchaseSucceeded(Product product) {
        if (onPurchaseSucceeded != null) {
            onPurchaseSucceeded.Invoke(product);
        }
    }

    void OnPurchaseFailed(Product product, PurchaseFailureReason reason) {
        button.interactable = true;
        if (onPurchaseFailed != null) {
            onPurchaseFailed.Invoke(product, reason);
        }
    }
}
#endif