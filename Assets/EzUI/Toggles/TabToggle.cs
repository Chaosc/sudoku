﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabToggle : MonoBehaviour {

    public GameObject[] contents;

    private Toggle toggle;

    void Awake() {
        toggle = GetComponent<Toggle>();
    }

	// Use this for initialization
	void Start() {
        toggle.interactable = !toggle.isOn;
        foreach (GameObject content in contents) {
            content.SetActive(toggle.isOn);
        }
        toggle.onValueChanged.AddListener(OnValueChanged);
	}
	
	void OnValueChanged(bool value) {
        toggle.interactable = !value;
        foreach (GameObject content in contents) {
            content.SetActive(value);
        }
    }
}
