﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Tap : Singleton<Tap> {

    public float allowance = 0.1f;
    public float initTime = 2f;
    public RectTransform outer;
    public RectTransform inner;
    public Text counter;
    public Image correct;

    [System.Serializable]
    public class HitEvent : UnityEvent { }

    public HitEvent onHit;

    [System.Serializable]
    public class MissEvent : UnityEvent { }

    public MissEvent onMiss;

    public float speed {
        get { return 1 / scaleTime; }
        set { scaleTime = 1 / value; }
    }

    private float oldSpeed;

    private float hitScale;
    private float scaleTime;
    private Image image;
    private Image outerImage;
    private Image innerImage;
    private Animator counterAnimator;
    private int hits;

    void Awake() {
        hitScale = inner.sizeDelta.x / outer.sizeDelta.x;
        scaleTime = initTime;
        image = GetComponent<Image>();
        outerImage = outer.GetComponent<Image>();
        innerImage = inner.GetComponent<Image>();
        counterAnimator = counter.GetComponent<Animator>();
        hits = 0;
    }

    public void Show() {
        Debug.Log("Show: speed=" + speed);
        innerImage.enabled = true;
        outerImage.enabled = true;
        outer.localScale = Vector3.one;
        iTween.Stop(outer.gameObject);
        image.enabled = true;
        Scale();
    }

    void Scale() {
        iTween.ScaleTo(outer.gameObject, iTween.Hash("scale", Vector3.zero, "time", scaleTime,
            "easetype", iTween.EaseType.easeInSine, "oncomplete", "OnTap", "oncompletetarget", gameObject));
    }

    public void OnGuideEnd(string action) {
        if (action == "Cancel") {
            Scale();
        }
    }

    public void OnTap() {
        if (!image.enabled) return;
        image.enabled = false;
        Debug.Log("OnTap");
        if (Mathf.Abs(outer.localScale.x - hitScale) < allowance) {
            OnHit();
        } else {
            OnMiss();
        }
        iTween.Stop(outer.gameObject);
        Invoke("Reset", 0.5f);
    }

    public void OnHit() {
        ++hits;
        correct.enabled = true;
        counter.text = hits.ToString();
        counter.enabled = true;
        counterAnimator.SetTrigger("Hit");
        if (onHit != null) {
            onHit.Invoke();
        }
        GameManager.instance.PlaySound("tap_hit");
    }

    public void OnMiss() {
        Finish();
        if (onMiss != null) {
            onMiss.Invoke();
        }
        GameManager.instance.PlaySound("tap_miss");
    }

    void Reset() {
        Debug.Log("Reset");
        outerImage.enabled = false;
        innerImage.enabled = false;
        correct.enabled = false;
        outer.localScale = Vector3.one;
    }

    public void Finish() {
        hits = 0;
        scaleTime = initTime;
        counter.enabled = false;
        counter.text = hits.ToString();
    }
}
