﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ProgressImage : MonoBehaviour {

    public float currentValue;
    public float maxValue;
    public float changeSpeed = 10;

    [System.Serializable]
    public class ColorSection : Section<Color, float> { }

    public ColorSection[] colorSections;

    [System.Serializable]
    public class ChangeEvent : UnityEvent<float> { }
    public ChangeEvent onChange;

    public float progress { get { return value / maxValue; } }
    public Color progressColor { get { return ColorSection.GetValue(colorSections, progress, FloatCondition.NoLess, originColor); } }
    public Color color { get { return image.color; } }

    private const float TOL = 1e-3f;

    private Image image {
        get {
            if (_image == null) {
                _image = GetComponent<Image>();
            }
            return _image;
        }
    }
    private Image _image;
    private Color originColor;
    private float value;
    private float maxDiff;

    void Awake() {
        originColor = image.color;
    }

    // Update is called once per frame
    void Update() {
        if (maxValue > 0) {
            float diff = Mathf.Abs(value - currentValue);
            maxDiff = Mathf.Max(maxDiff, diff);
            if (diff < maxDiff * TOL) {
                if (diff > 0) {
                    value = currentValue;
                    if (onChange != null) {
                        onChange.Invoke(value);
                    }
                }
            } else {
                value = Mathf.Lerp(value, currentValue, Time.deltaTime * changeSpeed);
            }
            image.fillAmount = progress;
            image.color = progressColor;
        }
    }

    public void SetProgress(float current, float max) {
        value = currentValue = Mathf.Clamp(current, 0f, max);
        maxValue = max;
    }
}
