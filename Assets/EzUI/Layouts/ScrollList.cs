﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollList : MonoBehaviour {

    public GameObject itemPrefab;
    public int itemCount = 1;
    public RectTransform snapCenter;
    public float snapSpeed = 10f;
    public float stopSpeed = 1000f;
    public float minScale = 0.75f;
    public float maxScale = 1.0f;
    public int initIndex;
    public int currentIndex { get; protected set; }

    public Button backwardButton;
    public Button forwardButton;

    public bool scrolling { get { return scrollRect.velocity.magnitude >= stopSpeed; } }

    protected RectTransform content;
    protected LayoutGroup layoutGroup;
    protected RectTransform item;
    protected ScrollRect scrollRect;

    protected int scrollIndex = -1;
    protected float scrollSpeed;

    protected bool reversed;
    protected bool dragging;

    // Use this for initialization
    void Start() {
        content = GetComponent<RectTransform>();
        layoutGroup = GetComponent<LayoutGroup>();
        if (itemPrefab != null) {
            item = itemPrefab.GetComponent<RectTransform>();
        } else {
            item = content.GetChild(0).GetComponent<RectTransform>();
        }
        scrollRect = GetComponentInParent<ScrollRect>();
        int exists = transform.childCount;
        if (scrollRect.horizontal && scrollRect.vertical) {
            Debug.LogError("ScrollList cannot support to scroll both horizontal and vertical");
        } else if (scrollRect.horizontal) {
            for (int i = 0; i < itemCount; ++i) {
                ScrollListItem child = (i < exists)
                    ? transform.GetChild(i).GetComponent<ScrollListItem>()
                    : Instantiate(item.gameObject).GetComponent<ScrollListItem>();
                if (i >= exists) child.transform.SetParent(transform, false);
                child.Init(i);
            }
        } else if (scrollRect.vertical) {
            for (int i = 0; i < itemCount; ++i) {
                ScrollListItem child = (i < exists)
                    ? transform.GetChild(i).GetComponent<ScrollListItem>()
                    : Instantiate(item.gameObject).GetComponent<ScrollListItem>();
                if (i >= exists) child.transform.SetParent(transform, false);
                child.Init(i);
            }
        }
        if (backwardButton != null) {
            backwardButton.onClick.AddListener(ScrollBackward);
        }
        if (forwardButton != null) {
            forwardButton.onClick.AddListener(ScrollForward);
        }
        Scroll(initIndex);
    }

    // Update is called once per frame
    void Update() {
        Fill();
        int snapIndex = -1;
        if (snapCenter != null) {
            float minDistance = float.MaxValue;
            float distance = minDistance;
            for (int i = 0; i < transform.childCount; ++i) {
                RectTransform child = transform.GetChild(i).GetComponent<RectTransform>();
                child.GetComponent<CanvasGroup>().interactable = false;
                float scale = maxScale;
                if (scrollRect.horizontal) {
                    distance = Mathf.Abs(snapCenter.position.x - child.position.x);
                    float size = snapCenter.sizeDelta.x * snapCenter.lossyScale.x;
                    if (minScale != maxScale) {
                        scale = Mathf.Max(minScale, maxScale - (maxScale - minScale) * distance / size);
                    }
                } else if (scrollRect.vertical) {
                    distance = Mathf.Abs(snapCenter.position.y - child.position.y);
                    float size = snapCenter.sizeDelta.y * snapCenter.lossyScale.y;
                    if (minScale != maxScale) {
                        scale = Mathf.Max(minScale, maxScale - (maxScale - minScale) * distance / size);
                    }
                }
                child.localScale = new Vector3(scale, scale, 1f);
                if (distance < minDistance) {
                    minDistance = distance;
                    snapIndex = i;
                }
            }
        }
        if (scrollIndex >= 0) {
            transform.GetChild(scrollIndex).GetComponent<CanvasGroup>().interactable = true;
            ScrollToTarget();
            if (snapIndex == scrollIndex) {
                scrollIndex = -1;
            }
        } else if (snapIndex >= 0) {
            transform.GetChild(snapIndex).GetComponent<CanvasGroup>().interactable = true;
            if (!dragging && scrollRect.velocity.magnitude < stopSpeed) {
                ScrollToSnap(snapIndex);
            }
        }
    }

    bool Fill() {
        if (transform.childCount < itemCount) {
            ScrollListItem newItem = Instantiate(item.gameObject).GetComponent<ScrollListItem>();
            newItem.transform.SetParent(transform, false);
            int index = transform.childCount - 1;
            if (scrollRect.horizontal) {
                newItem.Init(index);
            } else if (scrollRect.vertical) {
                newItem.Init(index);
            }
            return true;
        }
        return false;
    }

    public bool CanScrollBackward() {
        return currentIndex > 0;
    }

    public bool CanScrollForward() {
        return currentIndex < itemCount - 1;
    }

    public void ScrollBackward() {
        if (CanScrollBackward()) {
            Scroll(currentIndex - 1, snapSpeed);
        }
    }

    public void ScrollForward() {
        if (CanScrollForward()) {
            Scroll(currentIndex + 1, snapSpeed);
        }
    }

    public void Scroll(int index, float speed = 0) {
        if (index >= 0 && index < transform.childCount) {
            scrollIndex = index;
            scrollSpeed = speed;
        }
    }

    private void ScrollToTarget() {
        if (scrollIndex >= 0 && scrollIndex < transform.childCount) {
            RectTransform item = transform.GetChild(scrollIndex).GetComponent<RectTransform>();
            Scroll(item, scrollSpeed);
        }
    }

    private void ScrollToSnap(int snapIndex) {
        if (snapIndex >= 0 && snapIndex < transform.childCount) {
            currentIndex = snapIndex;
            if (backwardButton != null) {
                backwardButton.gameObject.SetActive(CanScrollBackward());
            }
            if (forwardButton != null) {
                forwardButton.gameObject.SetActive(CanScrollForward());
            }
            RectTransform item = transform.GetChild(snapIndex).GetComponent<RectTransform>();
            Scroll(item, snapSpeed);
        }
    }

    private void Scroll(RectTransform item, float speed = 0) {
        if (item != null) {
            if (scrollRect.horizontal) {
                float newX = layoutGroup.padding.left - item.anchoredPosition.x + item.sizeDelta.x / 2;
                if (speed > 0) {
                    content.anchoredPosition = Vector2.Lerp(content.anchoredPosition,
                        content.anchoredPosition.NewX(newX), Time.deltaTime * speed);
                } else {
                    content.anchoredPosition = content.anchoredPosition.NewX(newX);
                }
            } else if (scrollRect.vertical) {
                float newY = -layoutGroup.padding.bottom - item.anchoredPosition.y - item.sizeDelta.y / 2;
                if (speed > 0) {
                    content.anchoredPosition = Vector2.Lerp(content.anchoredPosition,
                        content.anchoredPosition.NewY(newY), Time.deltaTime * snapSpeed);
                } else {
                    content.anchoredPosition = content.anchoredPosition.NewY(newY);
                }
            }
        }
    }

    public void BeginDrag() {
        dragging = true;
        scrollIndex = -1;
    }

    public void EndDrag() {
        dragging = false;
        scrollIndex = -1;
    }
}
