﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : Panel {


    public Text CurrentTime;
    public Text BestTime;

    protected override void OnAwake() {
        base.OnAwake();
        BestTime.text = Localization.GetMultilineText("BestTime")+"    " + GameController.instance.CountTime((long)GameManager.instance.bestScore);
        CurrentTime.text = Localization.GetMultilineText("CurrentTime") +"    "+ GameController.instance.CountTime((long)GameManager.instance.score);
        GameManager.instance.StopMusic();
        GameManager.instance.PlaySound("win");
#if UNITY_ANDROID
        if (5 == GameManager.instance.currentLevel && GameManager.instance.currentLittleLevel == 200) {
            GameManager.instance.UnlockMaster();
        }
#endif
    }

    protected override void OnUpdate() {
        base.OnUpdate();
    }

    public void NewGame() {
        GameManager.instance.isGoLevelSelect = true;
        GameManager.instance.LoadScene("Home");
    }

    private void OnDestroy() {
        GameManager.instance.PlayMusic();
    }



}
