﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ShareRewardPanel : Panel {

	protected override void OnStart() {
		base.OnStart();
		transform.Find("InnerPanel/Title").GetComponent<Text>().text = EzRemoteConfig.shareRewardTitle;
		transform.Find("InnerPanel/Body").GetComponent<Text>().text = EzRemoteConfig.shareRewardBody;
		transform.Find("InnerPanel/Share/Text").GetComponent<Text>().text = EzRemoteConfig.shareRewardButton;
	}

	public void Share() {
		OnClick("Share");
		Close();
		GameManager.instance.ShareReward();
	}
}
