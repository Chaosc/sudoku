﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromoPanel : Panel {

	protected override void OnStart() {
		base.OnStart();
		GetComponentInChildren<WebImage>().Load(GameManager.instance.promoImage);
		cancelByBack = cancelByClickBlank = !EzRemoteConfig.promoForce;
	}

	public void Download() {
		OnClick("Download");
		GameManager.instance.GotoPromo();
		Close();
	}
}
