﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class BestScore : MonoBehaviour {

    public string format = "{0:D}";
    public string formatKey;

    private IntText intText;
    private Text text;
    private ContentSizeFitter fitter;

    void Awake() {
        intText = GetComponent<IntText>();
        fitter = GetComponent<ContentSizeFitter>();
        if (intText != null) {
            intText.format = format;
            intText.formatKey = formatKey;
        } else {
            text = GetComponent<Text>();
            if (text != null) {
                if (!string.IsNullOrEmpty(formatKey)) {
                    format = Localization.GetMultilineText(formatKey);
                }
            }
        }
        Update();
    }

    // Update is called once per frame
    void Update() {
        if (intText != null) {
            intText.targetValue = GameManager.instance.bestScore;
        } else if (text != null) {
            text.text = string.Format(format, GameManager.instance.bestScore);
        }
        if (fitter != null) {
            fitter.SetLayoutHorizontal();
        }
    }
}
