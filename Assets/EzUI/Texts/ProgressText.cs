﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ProgressText : MonoBehaviour {

    public string format = "{0:F0}/{1:F1}";
    public string formatKey;

    public float currentValue;
    public float maxValue;
    public float changeSpeed = 20;

    public float progress { get { return value / maxValue; } }

    [System.Serializable]
    public class ChangeEvent : UnityEvent<float> { }
    public ChangeEvent onChange;

    private const float TOL = 1e-5f;

    private Text text;
    private float value;

    void Awake() {
        text = GetComponent<Text>();
        if (!string.IsNullOrEmpty(formatKey)) {
            format = Localization.GetMultilineText(formatKey);
            formatKey = "";
        }
    }

    // Update is called once per frame
    void Update() {
        float diff = Mathf.Abs(value - currentValue);
        if (diff < Mathf.Abs(value) * TOL) {
            if (diff > 0) {
                value = currentValue;
                text.text = string.Format(format, value, maxValue);
                if (onChange != null) {
                    onChange.Invoke(value);
                }
            }
        } else {
            value = Mathf.Lerp(value, currentValue, Time.deltaTime * changeSpeed);
            text.text = string.Format(format, value, maxValue);
        }
    }

    public void SetProgress(float current) {
        value = currentValue = Mathf.Clamp(current, 0f, maxValue);
    }

    public void SetProgress(float current, float max) {
        maxValue = max;
        value = currentValue = Mathf.Clamp(current, 0f, max);
    }
}
