﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LowerCaseInput : MonoBehaviour {

    private InputField input;

    void Awake() {
        input = GetComponent<InputField>();
        input.onValidateInput = OnValidateInput;
    }

    char OnValidateInput(string text, int charIndex, char addedChar) {
        return addedChar.ToString().ToLower().ToCharArray()[0];
    }
}
