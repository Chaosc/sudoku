﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class IntText : MonoBehaviour {

    public string format = "{0:D}";
    public string formatKey;

    public int targetValue;
    public float changeSpeed = 10;

    [System.Serializable]
    public class ChangeEvent : UnityEvent<int> { }
    public ChangeEvent onChange;

    private const float TOL = 1e-5f;

    private Text text;
    private int value;
    private float current;

	void Awake() {
        text = GetComponent<Text>();
        if (!string.IsNullOrEmpty(formatKey)) {
            format = Localization.GetMultilineText(formatKey);
            formatKey = "";
        }
    }
	
	// Update is called once per frame
	void Update() {
        float diff = Mathf.Abs(value - targetValue);
        if (diff < Mathf.Abs(value) * TOL) {
            if (diff > 0) {
                value = targetValue;
                text.text = string.Format(format, value);
                if (onChange != null) {
                    onChange.Invoke(targetValue);
                }
            }
        } else {
            current = Mathf.Lerp(current, targetValue, Time.deltaTime * changeSpeed);
            value = Mathf.RoundToInt(current);
            text.text = string.Format(format, value);
        }
    }

    public void SetValue(int value) {
        targetValue = value;
        current = value;
        this.value = value;
    }
}
