﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCoin : MonoBehaviour {

    public bool updatePerFrame = true;
    private IntText intText;
    private Text text;

    void Awake() {
        intText = GetComponent<IntText>();
        if (intText == null) {
            text = GetComponent<Text>();
        }
    }

    void OnEnable() {
        if (intText != null) {
            intText.SetValue(GameManager.instance.coins);
        } else if (text != null) {
            text.text = GameManager.instance.coins.ToString();
        }
    }

    // Update is called once per frame
    void Update() {
        if (updatePerFrame) {
            if (intText != null) {
                intText.targetValue = GameManager.instance.coins;
            } else if (text != null) {
                text.text = GameManager.instance.coins.ToString();
            }
        }
    }
}
