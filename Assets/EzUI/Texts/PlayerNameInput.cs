﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameInput : MonoBehaviour {

    private InputField inputField;

    void Awake() {
        inputField = GetComponent<InputField>();
    }

    // Use this for initialization
    void Start() {
        inputField.onEndEdit.AddListener((text) => {
            if (!string.IsNullOrEmpty(text)) {
                GameManager.instance.UpdatePlayerName(text);
            }
        });
        inputField.text = GameManager.instance.playerName;
        if (string.IsNullOrEmpty(inputField.text)) {
#if UNITY_ANDROID && !NO_GPGS
            GameManager.instance.Authenticate((_) => {
                inputField.text = GameManager.instance.playerName;
            });
#else
            GameManager.instance.FixupPlayerName();
#endif
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
