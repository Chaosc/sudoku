using UnityEngine;
using System.Collections;


public class TweenChainGUI : BaseDemoGUI {
    // we have 4 cubes setup
    public Transform[] cubes;


    void Start() {
        // create a TweenConfig that we will use on all 4 cubes
        var config = new EzTweenConfig()
            .SetEaseType(EzEaseType.QuadIn) // set the ease type for the tweens
            .MaterialColor(Color.magenta) // tween the material color to magenta
			.Position(new Vector3(2, 8, 0), true) // relative position tween so it will be start from the current location
			.Rotation(new Vector3(0, 360, 0)) // do a 360 rotation
            .SetLoops(2, EzLoopType.PingPong); // 2 iterations with a PingPong loop so we go out and back

        // create the chain and set it to have 2 iterations
        var chain = new EzTweenChain(new EzTweenCollectionConfig().SetLoops(2));

        // add a completion handler for the chain
        chain.OnComplete(c => Debug.Log("chain complete"));

        // create a Tween for each cube and it to the chain
        foreach (var cube in cubes) {
            var tween = new EzTween(cube, 0.5f, config);
            chain.Append(tween);
        }

        _tween = chain;
    }

}
