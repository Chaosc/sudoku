﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITweenable<T> {

    T Diff(T start, T end);

    T Lerp(T start, T diff, float time);
	
}
