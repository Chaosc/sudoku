﻿using System;

public static class EzEaseSinusoidal {
    public static float In(float t, float b, float c, float d) {
        return -c * (float)Math.Cos(t / d * (Math.PI / 2)) + c + b;
    }

    public static float Out(float t, float b, float c, float d) {
        return c * (float)Math.Sin(t / d * (Math.PI / 2)) + b;
    }

    public static float InOut(float t, float b, float c, float d) {
        return -c / 2 * ((float)Math.Cos(Math.PI * t / d) - 1) + b;
    }
}