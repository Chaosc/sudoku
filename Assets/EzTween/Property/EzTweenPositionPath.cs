using UnityEngine;

public class EzTweenPositionPath : EzTweenVector3Path {

	protected Transform target;
	protected EzLookAtType lookAtType = EzLookAtType.None;
	protected Transform lookTarget;
	protected EzSmoothedQuaternion smoothedRotation;

	public EzTweenPositionPath(EzSpline path, bool relative = false, bool localSpace = false, 
	                           EzLookAtType lookAtType = EzLookAtType.None, Transform lookTarget = null)
		: base(localSpace ? "localPosition" : "position", path, relative) {
		this.lookAtType = lookAtType;
		this.lookTarget = lookTarget;
	}

	public override void Start() {
		target = owner.target as Transform;
		base.Start();

		// validate the lookTarget if we are set to look at it
		if (lookAtType == EzLookAtType.TargetTransform) {
			if (lookTarget == null) {
				lookAtType = EzLookAtType.None;
			}
		}
		
		// prep our smoothed rotation
		smoothedRotation = target.rotation;
	}

    public override void Tick(float elapsed) {
        var easedTime = easeFunction(elapsed, 0, 1, owner.duration);
        var position = path.GetPointOnPath(easedTime);
        position = relative ? position + startValue : position;
        var current = property.Get(owner.target);
        // handle look types
        switch (lookAtType) {
            case EzLookAtType.NextPathNode:
                smoothedRotation.smoothValue = position.Equals(current) ? Quaternion.identity :
                    Quaternion.LookRotation(position - current);
                target.rotation = smoothedRotation.smoothValue;
                break;
            case EzLookAtType.TargetTransform:
                target.LookAt(lookTarget, Vector3.up);
                break;
        }
        property.Set(owner.target, position);
    }

}
