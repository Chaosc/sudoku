﻿using UnityEngine;

public class EzTweenVector2Path : TweenProperty {

    public string Name { get; protected set; }

    protected Property<Vector2> property;

    protected EzSpline path;
    protected Vector3 startValue;

    public EzTweenVector2Path(string name, EzSpline path, bool relative = false)
        : base(relative) {
        Name = name;
        this.path = path;
    }

    public override bool ValidateTarget(object target) {
        property = new Property<Vector2>(Name, target.GetType());
        return property.Valid;
    }

    public override void Start() {
        // if this is a from tween first reverse the path then build it. we unreverse in case we were copied
        if (owner.fromMode)
            path.ReverseNodes();
        else
            path.UnreverseNodes();

        path.BuildPath();

        // a from tween means the start value is the last node
        if (owner.fromMode) {
            startValue = path.GetLastNode();
        } else {
            startValue = property.Get(owner.target);
        }
    }

    public override void Tick(float elapsed) {
        var easedTime = easeFunction(elapsed, 0, 1, owner.duration);
        Vector3 point = path.GetPointOnPath(easedTime);
        var result = relative ? point + startValue : point;
        property.Set(owner.target, result);
    }
}
