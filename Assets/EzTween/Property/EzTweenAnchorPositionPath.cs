using UnityEngine;

public class EzTweenAnchorPositionPath : EzTweenVector2Path {

	protected RectTransform target;
	protected EzLookAtType lookAtType = EzLookAtType.None;
	protected RectTransform lookTarget;
	protected EzSmoothedQuaternion smoothedRotation;

	public EzTweenAnchorPositionPath(EzSpline path, bool relative = false, 
	                                 EzLookAtType lookAtType = EzLookAtType.None, RectTransform lookTarget = null)
		: base("anchorPosition", path, relative) {
		this.lookAtType = lookAtType;
		this.lookTarget = lookTarget;
	}

	public override void Start() {
		target = owner.target as RectTransform;
		base.Start();

		// validate the lookTarget if we are set to look at it
		if (lookAtType == EzLookAtType.TargetTransform) {
			if (lookTarget == null) {
				lookAtType = EzLookAtType.None;
			}
		}
		
		// prep our smoothed rotation
		smoothedRotation = target.rotation;
	}

    public override void Tick(float elapsed) {
        float easedTime = easeFunction(elapsed, 0, 1, owner.duration);
        Vector3 position = path.GetPointOnPath(easedTime);
        position = relative ? position + startValue : position;
        Vector3 current = property.Get(owner.target);
        // handle look types
        switch (lookAtType) {
            case EzLookAtType.NextPathNode:
                smoothedRotation.smoothValue = position.Equals(current) ? Quaternion.identity :
                    Quaternion.LookRotation(Vector3.forward, position - current);
                target.rotation = smoothedRotation.smoothValue;
                break;
            case EzLookAtType.TargetTransform:
                target.rotation = Quaternion.LookRotation(Vector3.forward, 
                    target.anchoredPosition.ToV3() - current);
                break;
        }
        property.Set(owner.target, position);
    }

}
