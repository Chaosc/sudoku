using UnityEngine;
using System.Collections;


/// <summary>
/// based on the idea by Mike Talbot here http://whydoidoit.com/2012/04/01/smoothed-vector3-quaternions-and-floats-in-unity/
///
/// lerps or slerps a Quaternion over time. usage is like so:
///
/// mySmoothedQuat = target.rotation; // creates the GoSmoothedQuaternion
/// mySmoothedQuat.smoothValue = someNewQuaternion; // update the smoothValue whenever you would normally set the value on your object
/// target.rotation = mySmoothedQuat.smoothValue; // use the smoothValue property in an Update method to lerp/slerp it
///
/// </summary>
public struct EzSmoothedQuaternion {
	public EzSmoothingType smoothingType;
	public float duration;
	
	private Quaternion currentValue;
	private Quaternion targetValue;
	private Quaternion startValue;
	private float startTime;
	
	public EzSmoothedQuaternion(Quaternion quaternion) {
		currentValue = quaternion;
		startValue = quaternion;		
		targetValue = quaternion;
		startTime = Time.time;
		
		// set sensible defaults
		duration = 0.2f;
		smoothingType = EzSmoothingType.Lerp;
	}

	public Quaternion smoothValue {
		get {
			// how far along are we?
			var t = (Time.time - startTime) / duration;
			
			switch (smoothingType) {
			case EzSmoothingType.Lerp:
				currentValue = Quaternion.Lerp(startValue, targetValue, t);
				break;
			case EzSmoothingType.Slerp:
				currentValue = Quaternion.Slerp(startValue, targetValue, t);
				break;
			}
			return currentValue;
		}
		set {
			startValue = smoothValue;
			startTime = Time.time;
			targetValue = value;
		}
	}

	public float x {
		get {
			return currentValue.x;
		}
		set {
			smoothValue = new Quaternion(value, targetValue.y, targetValue.z, targetValue.w);
		}
	}

	public float y {
		get {
			return currentValue.y;
		}
		set {
			smoothValue = new Quaternion(targetValue.x, value, targetValue.y, targetValue.w);
		}
	}

	public float z {
		get {
			return currentValue.z;
		}
		set {
			smoothValue = new Quaternion(targetValue.x, targetValue.y, value, targetValue.w);
		}
			
	}

	public float w {
		get {
			return currentValue.w;
		}
		set {
			
			smoothValue = new Quaternion(targetValue.x, targetValue.y, targetValue.z, value);
		}
	}
	
	public static implicit operator EzSmoothedQuaternion(Quaternion q) {
		return new EzSmoothedQuaternion(q);
	}
	
}
