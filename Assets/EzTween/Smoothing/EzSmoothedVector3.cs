using UnityEngine;
using System.Collections;

public struct EzSmoothedVector3 {
	public EzSmoothingType smoothingType;
	public float duration;
	
	private Vector3 currentValue;
	private Vector3 targetValue;
	private Vector3 startValue;
	private float startTime;

	
	public EzSmoothedVector3(Vector3 vector) {
		currentValue = vector;
		startValue = vector;		
		targetValue = vector;
		startTime = Time.time;
		
		// set sensible defaults
		duration = 0.2f;
		smoothingType = EzSmoothingType.Lerp;
	}

	
	public Vector3 smoothValue {
		get {
			// how far along are we?
			var t = (Time.time - startTime) / duration;
			
			switch (smoothingType) {
			case EzSmoothingType.Lerp:
				currentValue = Vector3.Lerp(startValue, targetValue, t);
				break;
			case EzSmoothingType.Slerp:
				currentValue = Vector3.Slerp(startValue, targetValue, t);
				break;
			}
			return currentValue;
		}
		private set {
			startValue = smoothValue;
			startTime = Time.time;
			targetValue = value;
		}
	}
	
	public float x {
		get {
			return currentValue.x;
		}
		set {
			smoothValue = new Vector3(value, targetValue.y, targetValue.z);
		}
	}

	public float y {
		get {
			return currentValue.y;
		}
		set {
			smoothValue = new Vector3(targetValue.x, value, targetValue.y);
		}
	}

	public float z {
		get {
			return currentValue.z;
		}
		set {
			smoothValue = new Vector3(targetValue.x, targetValue.y, value);
		}
			
	}
	
	public static implicit operator EzSmoothedVector3(Vector3 v) {
		return new EzSmoothedVector3(v);
	}
	
}
