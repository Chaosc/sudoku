using UnityEngine;
using System.Collections;


public enum EzLogLevel
{
	None,
	Info,
	Warn,
	Error
}
