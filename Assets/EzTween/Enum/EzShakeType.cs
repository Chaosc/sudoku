[System.Flags]
public enum EzShakeType {
    Position = (1 << 0),
	Rotation = (1 << 1),
    Scale = (1 << 2),
}