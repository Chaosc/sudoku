using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// base class for TweenChains and TweenFlows
/// </summary>
public class EzTweenCollection : EzTweenBase {

    /// <summary>
    /// data class that wraps an AbstractTween and its start time for the timeline
    /// </summary>
    protected class TweenFlowItem {
        public float startTime;
        public float endTime { get { return startTime + duration; } }
        public float duration;
        public EzTweenBase tween;

        public TweenFlowItem(float startTime, EzTweenBase tween) {
            this.tween = tween;
            this.startTime = startTime;
            this.duration = tween.totalDuration;
        }

        public TweenFlowItem(float startTime, float duration) {
            this.duration = duration;
            this.startTime = startTime;
        }
    }

    protected List<TweenFlowItem> flowItems = new List<TweenFlowItem>();

    public EzTweenCollection(EzTweenCollectionConfig config) {
        // allow events by default
        allowEvents = true;

        // setup callback bools
        initilized = false;
        started = false;

        // flag the onIterationStart event to fire.
        // as long as goTo is not called on this tween, the onIterationStart event will fire
        fireLoopBegin = true;

        // copy the TweenConfig info over
        id = config.id;
        loopType = config.loopType;
        loops = config.loops;
        updateType = config.updateType;
        timeScale = 1;
        state = State.Paused;

        onInit = config.onInit;
        onStart = config.onStart;
        onLoopBegin = config.onLoopBegin;
        onUpdate = config.onUpdate;
        onLoopEnd = config.onLoopEnd;
        onComplete = config.onComplete;

        EzTweenManager.AddTween(this);
    }

    /// <summary>
    /// returns a list of all Tweens with the given target in the collection
    /// technically, this should be marked as internal
    /// </summary>
    public List<EzTween> TweensWithTarget(object target) {
        List<EzTween> list = new List<EzTween>();
        for (int i = 0; i < flowItems.Count; ++i) {
            TweenFlowItem flowItem = flowItems[i];
            // skip TweenFlowItems with no target
            if (flowItem.tween == null)
                continue;

            // check Tweens first
            var tween = flowItem.tween as EzTween;
            if (tween != null && tween.target == target)
                list.Add(tween);

            // check for TweenCollections
            if (tween == null) {
                var tweenCollection = flowItem.tween as EzTweenCollection;
                if (tweenCollection != null) {
                    var tweensInCollection = tweenCollection.TweensWithTarget(target);
                    if (tweensInCollection.Count > 0)
                        list.AddRange(tweensInCollection);
                }
            }
        }
        return list;
    }

    #region AbstractTween overrides

    public override bool RemoveTweenProperty(TweenProperty property) {
        for (int i = 0; i < flowItems.Count; ++i) {
            TweenFlowItem flowItem = flowItems[i];
            // skip delay items which have no tween
            if (flowItem.tween == null)
                continue;

            if (flowItem.tween.RemoveTweenProperty(property))
                return true;
        }
        return false;
    }

    public override bool ContainsTweenProperty(TweenProperty property) {
        for (int i = 0; i < flowItems.Count; ++i) {
            TweenFlowItem flowItem = flowItems[i];
            // skip delay items which have no tween
            if (flowItem.tween == null)
                continue;

            if (flowItem.tween.ContainsTweenProperty(property))
                return true;
        }
        return false;
    }


    public override List<TweenProperty> AllTweenProperties() {
        var propList = new List<TweenProperty>();
        for (int i = 0; i < flowItems.Count; ++i) {
            TweenFlowItem flowItem = flowItems[i];
            // skip delay items which have no tween
            if (flowItem.tween == null)
                continue;

            propList.AddRange(flowItem.tween.AllTweenProperties());
        }
        return propList;
    }

    /// <summary>
    /// we are always considered valid because our constructor adds us to Go and we start paused
    /// </summary>
    public override bool IsValid() {
        return true;
    }

    /// <summary>
    /// resumes playback
    /// </summary>
    public override void Play() {
        base.Play();
        for (int i = 0; i < flowItems.Count; ++i) {
            TweenFlowItem flowItem = flowItems[i];
            if (flowItem.tween != null) {
                flowItem.tween.Play();
            }
        }
    }

    /// <summary>
    /// pauses playback
    /// </summary>
    public override void Pause() {
        base.Pause();
        for (int i = 0; i < flowItems.Count; ++i) {
            TweenFlowItem flowItem = flowItems[i];
            if (flowItem.tween != null)
                flowItem.tween.Pause();
        }
    }

    /// <summary>
    /// tick method. if it returns true it indicates the tween is complete
    /// </summary>
    public override bool Tick(float deltaTime) {
        if (!initilized)
            OnInit();

        if (!started)
            OnStart();

        if (fireLoopBegin)
            OnLoopBegin();

        // update the timeline and state.
        base.Tick(deltaTime);

        // get the proper elapsedTime if we're doing a PingPong
        var convertedElapsedTime = isLoopingBackOnPingPong ? duration - elapsedTime : elapsedTime;

        // used for iterating over flowItems below.
        TweenFlowItem flowItem = null;

        // if we iterated last frame and this flow restarts from the beginning, we now need to reset all
        // of the flowItem tweens to either the beginning or the end of their respective timelines
        // we also want to do this in the _opposite_ way that we would normally iterate on them
        // as the start value of a later flowItem may alter a property of an earlier flowItem.
        if (didIterateLastFrame && loopType == EzLoopType.RestartFromBeginning) {
            if (reversed || isLoopingBackOnPingPong) {
                for (int i = 0; i < flowItems.Count; ++i) {
                    flowItem = flowItems[i];

                    if (flowItem.tween == null)
                        continue;

                    var cacheAllow = flowItem.tween.allowEvents;
                    flowItem.tween.allowEvents = false;
                    flowItem.tween.Restart();
                    flowItem.tween.allowEvents = cacheAllow;
                }
            } else {
                for (int i = flowItems.Count - 1; i >= 0; --i) {
                    flowItem = flowItems[i];

                    if (flowItem.tween == null)
                        continue;

                    var cacheAllow = flowItem.tween.allowEvents;
                    flowItem.tween.allowEvents = false;
                    flowItem.tween.Restart();
                    flowItem.tween.allowEvents = cacheAllow;
                }
            }
        } else {
            if ((reversed && !isLoopingBackOnPingPong) || (!reversed && isLoopingBackOnPingPong)) {
                // if we are moving the tween in reverse, we should be iterating over the flowItems in reverse
                // to help properties behave a bit better.
                for (var i = flowItems.Count - 1; i >= 0; --i) {
                    flowItem = flowItems[i];

                    if (flowItem.tween == null)
                        continue;

                    // if there's been an iteration this frame and we're not done yet, we want to make sure
                    // this tween is set to play in the right direction, and isn't set to complete/paused.
                    if (didIterateLastFrame && state != State.Completed) {
                        if (!flowItem.tween.reversed)
                            flowItem.tween.Reverse();

                        flowItem.tween.Play();
                    }

                    if (flowItem.tween.state == State.Running && flowItem.endTime >= convertedElapsedTime) {
                        var convertedDeltaTime = Mathf.Abs(convertedElapsedTime - flowItem.startTime - flowItem.tween.totalElapsedTime);
                        flowItem.tween.Tick(convertedDeltaTime);
                    }
                }
            } else {
                for (int i = 0; i < flowItems.Count; ++i) {
                    flowItem = flowItems[i];

                    if (flowItem.tween == null)
                        continue;

                    // if there's been an iteration this frame and we're not done yet, we want to make sure
                    // this tween is set to play in the right direction, and isn't set to complete/paused.
                    if (didIterateLastFrame && state != State.Completed) {
                        if (flowItem.tween.reversed)
                            flowItem.tween.Reverse();

                        flowItem.tween.Play();
                    }

                    if (flowItem.tween.state == State.Running && flowItem.startTime <= convertedElapsedTime) {
                        var convertedDeltaTime = convertedElapsedTime - flowItem.startTime - flowItem.tween.totalElapsedTime;
                        flowItem.tween.Tick(convertedDeltaTime);
                    }
                }
            }
        }

        OnUpdate();

        if (fireLoopEnd)
            OnLoopEnd();

        if (state == State.Completed) {
            OnComplete();
            return true; // true if complete
        }
        return false; // false if not complete
    }

    /// <summary>
    /// reverses playback. if going forward it will be going backward after this and vice versa.
    /// </summary>
    public override void Reverse() {
        base.Reverse();

        var convertedElapsedTime = isLoopingBackOnPingPong ? duration - elapsedTime : elapsedTime;
        foreach (var flowItem in flowItems) {
            if (flowItem.tween == null)
                continue;

            if (reversed != flowItem.tween.reversed)
                flowItem.tween.Reverse();

            flowItem.tween.Pause();

            // we selectively mark tweens for play if they will be played immediately or in the future.
            // update() will filter out more tweens that should not be played yet.
            if (reversed || isLoopingBackOnPingPong) {
                if (flowItem.startTime <= convertedElapsedTime)
                    flowItem.tween.Play();
            } else {
                if (flowItem.endTime >= convertedElapsedTime)
                    flowItem.tween.Play();
            }
        }
    }

    /// <summary>
    /// goes to the specified time clamping it from 0 to the total duration of the tween. if the tween is
    /// not playing it will be force updated to the time specified.
    /// </summary>
	public override void GoTo(float time, bool skipDelay = true) {
        time = Mathf.Clamp(time, 0f, totalDuration);

        // provide an early out for calling goTo on the same time multiple times.
        if (time == totalElapsedTime)
            return;

        // we don't simply call base.goTo because that would force an update within AbstractGoTweenCollection,
        // which forces an update on all the tweenFlowItems without putting them in the right position.
        // it's also possible that people will move around a tween via the goTo method, so we want to
        // try to make that as efficient as possible.

        // if we are doing a goTo at the "start" of the timeline, based on the isReversed variable,
        // allow the onBegin and onIterationStart callback to fire again.
        // we only allow the onIterationStart event callback to fire at the start of the timeline,
        // as doing a goTo(x) where x % duration == 0 will trigger the onIterationEnd before we
        // go to the start.
        if ((reversed && time == totalDuration) || (!reversed && time == 0f)) {
            started = false;
            fireLoopBegin = true;
        } else {
            started = true;
            fireLoopBegin = false;
        }

        // since we're doing a goTo, we want to stop this tween from remembering that it iterated.
        // this could cause issues if you caused the tween to complete an iteration and then goTo somewhere
        // else while still paused.
        didIterateThisFrame = false;

        // force a time and completedIterations before we update
        totalElapsedTime = time;
        completedLoops = reversed ? Mathf.CeilToInt(totalElapsedTime / duration) : Mathf.FloorToInt(totalElapsedTime / duration);

        // we don't want to use the Collection update function, because we don't have all of our
        // child tweenFlowItems setup properly. this will properly setup our iterations,
        // totalElapsedTime, and other useful information.
        base.Tick(0);

        var convertedElapsedTime = isLoopingBackOnPingPong ? duration - elapsedTime : elapsedTime;

        // we always want to process items in the future of this tween from last to first.
        // and items that have already occured from first to last.
        TweenFlowItem flowItem = null;
        if (reversed || isLoopingBackOnPingPong) {
            // flowItems in the future of the timeline
            for (int i = 0; i < flowItems.Count; ++i) {
                flowItem = flowItems[i];

                if (flowItem == null)
                    continue;

                if (flowItem.endTime >= convertedElapsedTime)
                    break;

                ChangeTimeForFlowItem(flowItem, convertedElapsedTime);
            }

            // flowItems in the past & current part of the timeline
            for (int i = flowItems.Count - 1; i >= 0; --i) {
                flowItem = flowItems[i];

                if (flowItem == null)
                    continue;

                if (flowItem.endTime < convertedElapsedTime)
                    break;

                ChangeTimeForFlowItem(flowItem, convertedElapsedTime);
            }
        } else {
            // flowItems in the future of the timeline
            for (int i = flowItems.Count - 1; i >= 0; --i) {
                flowItem = flowItems[i];

                if (flowItem == null)
                    continue;

                if (flowItem.startTime <= convertedElapsedTime)
                    break;

                ChangeTimeForFlowItem(flowItem, convertedElapsedTime);
            }

            // flowItems in the past & current part of the timeline
            for (int i = 0; i < flowItems.Count; ++i) {
                flowItem = flowItems[i];

                if (flowItem == null)
                    continue;

                if (flowItem.startTime > convertedElapsedTime)
                    break;

                ChangeTimeForFlowItem(flowItem, convertedElapsedTime);
            }
        }
    }

    private void ChangeTimeForFlowItem(TweenFlowItem flowItem, float time) {
        if (flowItem == null || flowItem.tween == null)
            return;

        if (flowItem.tween.reversed != (reversed || isLoopingBackOnPingPong))
            flowItem.tween.Reverse();

        var convertedTime = Mathf.Clamp(time - flowItem.startTime, 0f, flowItem.endTime);

        if (flowItem.startTime <= time && flowItem.endTime >= time) {
            flowItem.tween.GoToAndPlay(convertedTime);
        } else {
            flowItem.tween.GoTo(convertedTime);
            flowItem.tween.Pause();
        }
    }

    #endregion

}
