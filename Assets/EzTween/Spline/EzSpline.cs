// spline equation courtesy Andeeee's CRSpline (http://forum.unity3d.com/threads/32954-Waypoints-and-constant-variable-speed-problems?p=213942&viewfull=1#post213942)

using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;


public class EzSpline {
	public int CurrentSegment { get; private set; }

	public bool Closed { get; private set; }

	public EzSplineType Type { get; private set; }
	
	// used by the visual path editor
	public List<Vector3> Nodes { get { return solver.Nodes; } }

	private bool reversed;
	// internal flag that lets us know if our nodes are reversed or not
	private EzSplineSolver solver;
	
	// default constructor
	public EzSpline(List<Vector3> nodes, bool useStraightLines = false) {
		// determine spline type and solver based on number of nodes
		if (useStraightLines || nodes.Count == 2) {
			Type = EzSplineType.StraightLine;
			solver = new EzSplineStraightLineSolver(nodes);
		} else if (nodes.Count == 3) {
			Type = EzSplineType.QuadraticBezier;
			solver = new EzSplineQuadraticBezierSolver(nodes);
		} else if (nodes.Count == 4) {
			Type = EzSplineType.CubicBezier;
			solver = new EzSplineCubicBezierSolver(nodes);
		} else {
			Type = EzSplineType.CatmullRom;
			solver = new EzSplineCatmullRomSolver(nodes);
		}
	}

	public EzSpline(Vector3[] nodes, bool useStraightLines = false) : this(new List<Vector3>(nodes), useStraightLines) {
	}

	public EzSpline(string pathAssetName, bool useStraightLines = false) : this(NodeListFromAsset(pathAssetName), useStraightLines) {
	}
	
	/// <summary>
	/// helper to get a node list from an asset created with the visual editor
	/// </summary>
	private static List<Vector3> NodeListFromAsset(string pathAssetName) {
		if (Application.platform == RuntimePlatform.WebGLPlayer) {
			Debug.LogError("The Web Player does not support loading files from disk.");
			return null;
		}
		
		var path = string.Empty;
		if (!pathAssetName.EndsWith(".asset"))
			pathAssetName += ".asset";
		
		
		if (Application.platform == RuntimePlatform.Android) {
			path = Path.Combine("jar:file://" + Application.dataPath + "!/assets/", pathAssetName);
		
			WWW loadAsset = new WWW(path);
			while (!loadAsset.isDone) {
			} // maybe make a safety check here
			
			return BytesToVector3List(loadAsset.bytes);
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			// at runtime on iOS, we load from the dataPath
			path = Path.Combine(Path.Combine(Application.dataPath, "Raw"), pathAssetName);
		} else {
			// in the editor we default to looking in the StreamingAssets folder
			path = Path.Combine(Path.Combine(Application.dataPath, "StreamingAssets"), pathAssetName);
		}
		
#if UNITY_WEBPLAYER || NETFX_CORE
		// it isnt possible to get here but the compiler needs it to be here anyway
		return null;
#else
		var bytes = File.ReadAllBytes(path);
		return BytesToVector3List(bytes);
#endif
	}
	
	/// <summary>
	/// helper to get a node list from an asset created with the visual editor
	/// </summary>
	public static List<Vector3> BytesToVector3List(byte[] bytes) {
		var vecs = new List<Vector3>();
		for (var i = 0; i < bytes.Length; i += 12) {
			var newVec = new Vector3(System.BitConverter.ToSingle(bytes, i), System.BitConverter.ToSingle(bytes, i + 4), System.BitConverter.ToSingle(bytes, i + 8));
			vecs.Add(newVec);
		}
		return vecs;
	}
	
	/// <summary>
	/// gets the last node. used to setup relative tweens
	/// </summary>
	public Vector3 GetLastNode() {
		return solver.Nodes[solver.Nodes.Count];
	}
	
	/// <summary>
	/// responsible for calculating total length, segmentStartLocations and segmentDistances
	/// </summary>
	public void BuildPath() {
		solver.BuildPath();
	}
	
	/// <summary>
	/// directly gets the point for the current spline type with no lookup table to adjust for constant speed
	/// </summary>
	private Vector3 GetPoint(float t) {
		return solver.GetPoint(t);
	}

	/// <summary>
	/// returns the point that corresponds to the given t where t >= 0 and t <= 1 making sure that the
	/// path is traversed at a constant speed.
	/// </summary>
	public Vector3 GetPointOnPath(float t) {
		// if the path is closed, we will allow t to wrap. if is not we need to clamp t
		if (t < 0 || t > 1) {
			if (Closed) {
				if (t < 0)
					t += 1;
				else
					t -= 1;
			} else {
				t = Mathf.Clamp01(t);
			}
		}
		return solver.GetPointOnPath(t);
	}
	
	/// <summary>
	/// closes the path adding a new node at the end that is equal to the start node if it isn't already equal
	/// </summary>
	public void ClosePath() {
		// dont let this get closed twice!
		if (Closed)
			return;
		
		Closed = true;
		solver.ClosePath();
	}

	/// <summary>
	/// reverses the order of the nodes
	/// </summary>
	public void ReverseNodes() {
		if (!reversed) {
			solver.ReverseNodes();
			reversed = true;
		}
	}
	
	/// <summary>
	/// unreverses the order of the nodes if they were reversed
	/// </summary>
	public void UnreverseNodes() {
		if (reversed) {
			solver.ReverseNodes();
			reversed = false;
		}
	}
	
	public void DrawGizmos(float resolution) {
		solver.DrawGizmos();
		
		var previousPoint = solver.GetPoint(0);
		
		resolution *= solver.Nodes.Count;
		for (var i = 1; i <= resolution; i++) {
			var t = (float)i / resolution;
			var currentPoint = solver.GetPoint(t);
			Gizmos.DrawLine(currentPoint, previousPoint);
			previousPoint = currentPoint;
		}
	}

	/// <summary>
	/// helper for drawing gizmos in the editor
	/// </summary>
	public static void DrawGizmos(Vector3[] path, float resolution = 50) {
		// horribly inefficient but it only runs in the editor
		var spline = new EzSpline(path);
		spline.DrawGizmos(resolution);
	}

}
