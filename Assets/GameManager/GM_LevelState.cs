﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public int testLevels = 0;
    public int maxLevels = 10;
    public int currentLevel = 0;
    public int currentLittleLevel = 0;
    public bool hasNextLevel { get { return currentLevel + 1 < maxLevels; } }

    public bool isGoDiffcultSelect = false;
    public bool isGoLevelSelect = false;

    public bool IsLevelUnlocked(int levelId) {
        return levelId < unlockedLevels;
    }

    public bool NextLevel() {
        if (IsLevelUnlocked(currentLevel + 1)) {
            currentLevel += 1;
            return true;
        }
        return false;
    }

    public void OnLevelStart() {
        EzAnalytics.LogEvent("Level" + currentLevel, "Start");
    }

    public void OnLevelComplete() {
        if (currentLevel + 1 == unlockedLevels) {
            UnlockLevel();
        }
        CheckAds();
        OpenLevelComplete();
    }

    public void OpenLevelComplete() {
        EzAnalytics.LogEvent("Level" + currentLevel, "Complete", score.ToString());
        Panel.Open("LevelCompletePanel");
    }

}
