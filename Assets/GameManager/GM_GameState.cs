﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public int onHomeTimes = 0;
    public void OnHome() {
        onHomeTimes++;
        CheckPromo();
    }

    private int reviveCount = 0;
    private float gameStartTime;
    private float interstitialTime = -1;
    public void OnGameStart() {
        reviveCount = 0;
        EzAnalytics.LogEvent("Game", "Start", playerName);
        gameStartTime = Time.realtimeSinceStartup;
        if (interstitialTime < 0) interstitialTime = Time.realtimeSinceStartup;
    }

    public void OnGameOver(bool revivable = false) {
        EzAnalytics.LogEvent("Game", "Over", score.ToString());
        if (revivable && CheckRevive()) return;
        GameOver();
    }

    public int gameOverTimes = 0;
    public void GameOver() {
        ++gameOverTimes;
        if (!CheckRate()) {
            EzTiming.CallDelayed(2f, () => {
                CheckAds();
            });
            OpenGameOver();
        }

    }

    public void OpenGameOver() {
        Panel.Open("GameOverPanel");
    }

    public bool CheckRevive() {
        if (Time.realtimeSinceStartup - gameStartTime > EzRemoteConfig.reviveMinTime &&
            (EzRemoteConfig.reviveMaxCount == 0 || reviveCount++ < EzRemoteConfig.reviveMaxCount) &&
            EzAds.IsRewardedVideoReady()) {
            OpenRevive();
            return true;
        }
        return false;
    }

    public void OpenRevive() {
        Panel.Open("RevivePanel");
    }

    public bool CheckAds(System.Action next = null) {
        if (!noAds && !EzRemoteConfig.noAds &&
            (Time.realtimeSinceStartup - interstitialTime > EzRemoteConfig.adShowTimeInterval ||
            (gameOverTimes >= EzRemoteConfig.adShowFirst &&
            (gameOverTimes - EzRemoteConfig.adShowFirst) % EzRemoteConfig.adShowInterval == 0))) {
            interstitialTime = Time.realtimeSinceStartup;
            return EzAds.ShowInterstitial();
        }
        return false;
    }

}
