﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    [System.Serializable]
    public class PromoRewardResult {
        public bool complete;
    }

    [System.Serializable]
    public class PromoRewardData {
        public int code;
        public string msg;
        public PromoRewardResult res;
    }

    private int promoIndex;

    public string promoId { get { return EzRemoteConfig.promoIds[promoIndex]; } }
    public string promoUrl { get { return EzRemoteConfig.promoUrls[promoIndex]; } }
    public string promoImage { get { return EzRemoteConfig.promoImages[promoIndex]; } }

    private int[] promoCounts;
    public int promoCount {
        get { return promoCounts[promoIndex]; }
        set { promoCounts[promoIndex] = value; }
    }

    private const string promoApiUrl = "http://share.game.adesk.com/excitation";
    public string promoRewardUrl { get { return promoApiUrl + uidParams; } }
    public string promoRewardMarkUrl { get { return promoApiUrl + uidParams + "&type=setComplete"; } }

    public void OpenPromo() {
        Panel.Open("PromoPanel");
    }

    public bool CheckPromo() {
        if (EzRemoteConfig.online &&
            onHomeTimes >= EzRemoteConfig.promoFirst &&
            (onHomeTimes - EzRemoteConfig.promoFirst) % EzRemoteConfig.promoInterval == 0) {
            for (; promoIndex < promoCounts.Length; ++promoIndex) {
                if (!string.IsNullOrEmpty(promoUrl) &&
                    !EzIds.bundleID.Equals(promoId) &&
                    promoCount < EzRemoteConfig.promoMaxCount) {
                    ++promoCount;
                    OpenPromo();
                    return true;
                }
            }
        }
        return false;
    }


    public void CheckPromoReward() {
        for (int i = 0; i < EzRemoteConfig.promoIds.Length; ++i) {
            string targetId = EzRemoteConfig.promoIds[i];
            if (string.IsNullOrEmpty(targetId)) continue;
            if (EzPrefs.GetBool(targetId)) continue;
            StartCoroutine(EzRestApi.Get(promoRewardUrl + "&topackage=" + EzCrypto.MD5(targetId), (json) => {
                PromoRewardData data = JsonUtility.FromJson<PromoRewardData>(json);
                if (data.code == 0 && data.res.complete) {
                    OnPromoRewardSucceeded(targetId);
                } else {
                    OnPromoRewardFailed(targetId);
                }
            }));
        }
    }

    void OnPromoRewardSucceeded(string targetId) {
        EzPrefs.SetBool(targetId, true);
        EzAnalytics.LogEvent("PromoReward", targetId, "Succeeded");
    }

    void OnPromoRewardFailed(string targetId) {
        EzAnalytics.LogEvent("PromoReward", targetId, "Failed");
    }

    public void GotoPromo() {
        if (string.IsNullOrEmpty(promoUrl)) return;
        EzAnalytics.LogEvent("Promo", "Goto", promoId);
        Application.OpenURL(promoUrl);
    }

    public void GotoPromoReward(int index) {
        if (index >= EzRemoteConfig.promoIds.Length) {
            Debug.LogWarning("Invalid promo index: " + index);
            return;
        }
        string targetId = EzRemoteConfig.promoIds[index];
        string targetUrl = EzRemoteConfig.promoUrls[index];
        if (string.IsNullOrEmpty(targetId) || string.IsNullOrEmpty(targetUrl)) return;
        StartCoroutine(EzRestApi.Post(promoRewardUrl + "&topackage=" + EzCrypto.MD5(targetId), (json) => {
            PromoRewardData data = JsonUtility.FromJson<PromoRewardData>(json);
            if (data.code == 0 && data.res.complete) {
                OnPromoRewardSucceeded(targetId);
            } else {
                EzAnalytics.LogEvent("PromoReward", targetId, "Goto");
                Application.OpenURL(targetUrl);
            }
        }));
    }

}
