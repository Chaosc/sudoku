﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public Dictionary<string, GameObject> prefabs = new Dictionary<string, GameObject>();
    void LoadPrefabs() {
        foreach (GameObject prefab in Resources.LoadAll<GameObject>("Prefabs")) {
            prefabs.Add(prefab.name, prefab);
        }
    }

    public GameObject LoadPrefab(string path) {
        if (prefabs.ContainsKey(path)) {
            return prefabs[path];
        }
        GameObject prefab = Resources.Load<GameObject>(path);
        if (prefab != null) {
            prefabs.Add(path, prefab);
        }
        return prefab;
    }

    public void LoadPrefabAsync(string path, System.Action onLoaded = null) {
        if (prefabs.ContainsKey(path)) {
            if (onLoaded != null) {
                onLoaded.Invoke();
            }
        } else {
            StartCoroutine(DoLoadPrefabAsync(path, onLoaded));
        }
    }

    IEnumerator DoLoadPrefabAsync(string path, System.Action onLoaded = null) {
        ResourceRequest req = Resources.LoadAsync<GameObject>(path);
        yield return req;
        GameObject prefab = req.asset as GameObject;
        if (prefab != null) {
            if (prefabs.ContainsKey(path)) {
                Debug.LogWarning("Prefab of " + path + " is already loaded");
            } else {
                prefabs.Add(path, prefab);
            }
            if (onLoaded != null) {
                onLoaded.Invoke();
            }
        }
    }

    public GameObject Instantiate(string path) {
        GameObject prefab = LoadPrefab(path);
        if (prefab != null) {
            GameObject go = Instantiate(prefab);
            go.name = prefab.name;
            return go;
        }
        return null;
    }

    public T Instantiate<T>(string path) where T : Component {
        GameObject go = Instantiate(path);
        if (go != null) {
            return go.GetComponent<T>();
        }
        return default(T);
    }

    public Dictionary<string, Sprite> sprites;
    void LoadSprites() {
        sprites = new Dictionary<string, Sprite>();
        foreach (Sprite sprite in Resources.LoadAll<Sprite>("Sprites")) {
            if (sprites.ContainsKey(sprite.name)) {
                Debug.LogWarning("Duplicated sprite: " + sprite.name);
                continue;
            }
            sprites.Add(sprite.name, sprite);
        }
    }

    public Sprite LoadSprite(string path) {
        if (sprites.ContainsKey(path)) {
            return sprites[path];
        }
        Sprite sprite = Resources.Load<Sprite>(path);
        if (sprites != null) {
            sprites.Add(path, sprite);
        }
        return sprite;
    }

    public LocalFont[] fonts;
    public Dictionary<string, Font> localFonts;
    public Font localFont {
        get {
            string language = Localization.GetLanguage();
            if (localFonts.ContainsKey(language)) {
                return localFonts[language];
            }
            return null;
        }
    }
    void LoadFonts() {
        localFonts = new Dictionary<string, Font>();
        foreach (LocalFont font in fonts) {
            localFonts.Add(font.language.ToUpper(), font.font);
        }
    }

    public string[] nicknames { get; set; }
    public int randomNameIndex { get { return Random.Range(0, nicknames.Length); } }
    public string randomNickname { get { return GetNickname(randomNameIndex); } }
    public string GetNickname(int index) {
        return nicknames[index % nicknames.Length];
    }
    void LoadTexts() {
        TextAsset ta = Resources.Load<TextAsset>("Texts");
        if (ta != null) {
            nicknames = ta.text.Split('\n');
        }
    }

}
