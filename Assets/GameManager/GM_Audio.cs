﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public float soundInterval = 0.05f;
    private AudioSource musicSource;
    private AudioSource soundSource;
    private Dictionary<string, AudioClip> sounds;
    private Dictionary<AudioClip, float> soundTimes;

    public bool muteMusic {
        get { return musicSource.mute; }
        set { musicSource.mute = value; }
    }

    public bool muteSound {
        get { return soundSource.mute; }
        set { soundSource.mute = value; }
    }

    void LoadSounds() {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        musicSource = soundSource = audioSources[0];
        if (audioSources.Length > 1) soundSource = audioSources[1];
        AudioClip[] clips = Resources.LoadAll<AudioClip>("Sounds");
        sounds = new Dictionary<string, AudioClip>();
        soundTimes = new Dictionary<AudioClip, float>();
        foreach (AudioClip clip in clips) {
            sounds.Add(clip.name, clip);
            soundTimes.Add(clip, 0);
        }
    }

    public void PlaySound(string name, float volumeScale = 1.0f) {
        if (sounds.ContainsKey(name)) {
            PlaySound(sounds[name], volumeScale);
        } else {
            Debug.LogWarning("Cannot find audio clip: " + name);
        }
    }

    public void PlaySound(AudioClip clip, float volumeScale) {
        if (!soundTimes.ContainsKey(clip)) {
            soundTimes.Add(clip, 0);
        }
        if (Time.time - soundTimes[clip] > soundInterval) {
            soundTimes[clip] = Time.time;
            soundSource.PlayOneShot(clip, volumeScale);
        }
    }

    public void PlaySound(AudioSource source, string name, float volumeScale = 1.0f) {
        if (sounds.ContainsKey(name)) {
            PlaySound(sounds[name], volumeScale);
        } else {
            Debug.LogWarning("Cannot find audio clip: " + name);
        }
    }

    public void PlaySound(AudioSource source, AudioClip clip, float volumeScale) {
        source.mute = soundSource.mute;
        if (!soundTimes.ContainsKey(clip)) {
            soundTimes.Add(clip, 0);
        }
        if (Time.time - soundTimes[clip] > soundInterval) {
            soundTimes[clip] = Time.time;
            source.PlayOneShot(clip, volumeScale);
        }
    }

    public void PlayMusic() {
        if (!musicSource.isPlaying) {
            musicSource.Play();
        }
    }

    public void StopMusic() {
        if (musicSource.isPlaying) {
            musicSource.Stop();
        }
    }

}
