﻿#if UNITY_PURCHASING && USE_IAP

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public partial class GameManager : MonoBehaviour, IStoreListener {

    private ProductCatalog catalog;
	private ProductCatalogItem[] products;
    private IStoreController controller;
    private IExtensionProvider extensions;
    private class PurchaseEvent {
        public System.Action<Product> onPurchaseCompleted;
        public System.Action<Product, PurchaseFailureReason> onPurchaseFailed;
        public PurchaseEvent(System.Action<Product> onPurchaseCompleted, 
            System.Action<Product, PurchaseFailureReason> onPurchaseFailed) {
            this.onPurchaseCompleted = onPurchaseCompleted;
            this.onPurchaseFailed = onPurchaseFailed;
        }
    }
    private PurchaseEvent[] purchaseEvents;
    private Dictionary<string, int> productIdToIndex;
    private System.Action onRestoreSucceeded;
    private System.Action onRestoreFailed;

    void InitIAP() {
        catalog = ProductCatalog.LoadDefaultCatalog();
        products = new ProductCatalogItem[catalog.allProducts.Count];
        catalog.allProducts.CopyTo(products, 0);
        productIdToIndex = new Dictionary<string, int>();
        for (int i = 0; i < products.Length; ++i) {
            productIdToIndex.Add(products[i].id, i);
        }
        purchaseEvents = new PurchaseEvent[products.Length];
        StandardPurchasingModule module = StandardPurchasingModule.Instance();
        module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
        ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);
        foreach (var product in catalog.allProducts) {
            if (product.allStoreIDs.Count > 0) {
                var ids = new IDs();
                foreach (var storeID in product.allStoreIDs) {
                    ids.Add(storeID.id, storeID.store);
                }
                builder.AddProduct(product.id, product.type, ids);
            } else {
                builder.AddProduct(product.id, product.type);
            }
        }
        UnityPurchasing.Initialize(this, builder);
    }

    public bool HasProductInCatalog(string productId) {
        foreach (var product in catalog.allProducts) {
            if (product.id == productId) {
                return true;
            }
        }
        return false;
    }

    public void PurchaseProduct(int productIndex,
        System.Action<Product> onPurchaseCompleted = null,
        System.Action<Product, PurchaseFailureReason> onPurchaseFailed = null) {
        if (controller == null) {
            Debug.LogError("Purchase failed because Purchasing was not initialized correctly");
            return;
        }
        string productId = GetProductId(productIndex);
        if (string.IsNullOrEmpty(productId)) {
            Debug.LogError("Product id is empty!");
            return;
        }
        Panel.Open("LoadingPanel");
        purchaseEvents[productIndex] = new PurchaseEvent(onPurchaseCompleted, onPurchaseFailed);
        controller.InitiatePurchase(productId);
    }

    public void RestorePurchasedProducts(System.Action onRestoreSucceeded = null, System.Action onRestoreFailed = null) {
        this.onRestoreSucceeded = onRestoreSucceeded;
        this.onRestoreFailed = onRestoreFailed;
        if (Application.platform == RuntimePlatform.WSAPlayerX86 || Application.platform == RuntimePlatform.WSAPlayerX64 || Application.platform == RuntimePlatform.WSAPlayerARM) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<IMicrosoftExtensions>().RestoreTransactions();
        } else if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.tvOS) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<IAppleExtensions>().RestoreTransactions(OnTransactionsRestored);
        } else if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().androidStore == AndroidStore.SamsungApps) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<ISamsungAppsExtensions>().RestoreTransactions(OnTransactionsRestored);
        } else if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().androidStore == AndroidStore.CloudMoolah) {
            Panel.Open("LoadingPanel");
            extensions.GetExtension<IMoolahExtension>().RestoreTransactionID((restoreTransactionIDState) => {
                OnTransactionsRestored(restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown);
            });
        } else {
            Debug.LogWarning(Application.platform.ToString() + " is not a supported platform for the Codeless IAP restore button");
        }
    }

    public string GetProductId(int index) {
        if (index >= 0 && index < products.Length) {
            return products[index].id;
        }
        return "";
    }

    public int GetProductIndex(string productId) {
        if (productIdToIndex.ContainsKey(productId)) {
            return productIdToIndex[productId];
        }
        Debug.Log("Invalid product id: " + productId);
        return -1;
    }

    public Product GetProduct(string productId) {
        if (controller != null) {
            return controller.products.WithID(productId);
        }
        return null;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
        Debug.Log("Purchasing initialized.");
        this.controller = controller;
        this.extensions = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error) {
        Debug.LogError(string.Format("Purchasing failed to initialize. Reason: {0}", error.ToString()));
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) {
        OnPurchaseCompleted(e.purchasedProduct);
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason reason) {
        Panel.Close("LoadingPanel");
        string productId = product.definition.id;
        Debug.Log(string.Format("OnPurchaseFailed(Product {0}, PurchaseFailureReason {1})", productId, reason));
        EzAnalytics.LogEvent("Purchase", productId, reason.ToString());
        int index = GetProductIndex(productId);
        if (purchaseEvents[index] != null && purchaseEvents[index].onPurchaseFailed != null) {
            purchaseEvents[index].onPurchaseFailed(product, reason);
            purchaseEvents[index] = null;
        }
        GameManager.instance.noAds = false;
        GameManager.instance.CancelUnlockMaster();
    }

    public void OnPurchaseCompleted(Product product) {
        Panel.Close("LoadingPanel");
        string productId = product.definition.id;
        Debug.Log(string.Format("OnPurchaseCompleted(Product {0})", productId));
        EzAnalytics.LogEvent("Purchase", productId, "Succeeded");
        int index = GetProductIndex(productId);
        OnProductPurchased(index);
        if (purchaseEvents[index] != null && purchaseEvents[index].onPurchaseCompleted != null) {
            purchaseEvents[index].onPurchaseCompleted(product);
            purchaseEvents[index] = null;
        }
    }

    void OnTransactionsRestored(bool success) {
        EzAnalytics.LogEvent("Purchase", "Restore", success ? "Succeeded" : "Failed");
        Debug.Log("Transactions restored: " + success);
        Panel.Close("LoadingPanel");
        if (success) {
            if (onRestoreSucceeded != null) {
                onRestoreSucceeded.Invoke();
                onRestoreSucceeded = null;
            }
        } else {
            if (onRestoreFailed != null) {
                onRestoreFailed.Invoke();
                onRestoreFailed = null;
            }
        }
    }

    void OnProductPurchased(int index) {
        
    }
}

#endif
