﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ANDROID && !NO_GPGS
using GooglePlayGames.BasicApi;
using GooglePlayGames;
#endif

public partial class GameManager : MonoBehaviour {

    void InitGooglePlayGameService() {
#if UNITY_ANDROID && !NO_GPGS && SOCIAL
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = debug;
        PlayGamesPlatform.Activate();
#endif
    }

    public void Authenticate(System.Action<bool> onFinish = null) {
#if SOCIAL && !UNITY_EDITOR
        Social.localUser.Authenticate(success => {
            if (success) {
                if (string.IsNullOrEmpty(playerName)) {
                    UpdatePlayerName(Social.localUser.userName);
                }
            } else {
                Debug.Log("Authenticate failed!");
                FixupPlayerName();
            }
            if (onFinish != null) {
                onFinish.Invoke(success);
            }
        });
#endif
    }

    #region Achievement

    public const int superRockKills = 10;
    public const int superScissorsKills = 10;
    public const int superPaperKills = 10;
    public const int poisonHandKills = 10;
    public const int ghostHandKills = 10;

#if UNITY_ANDROID && !NO_GPGS
    public const string achievement_super_rock = "CgkI18ub7YINEAIQAw"; // <GPGSID>
    public const string achievement_super_scissors = "CgkI18ub7YINEAIQAg"; // <GPGSID>
    public const string achievement_super_paper = "CgkI18ub7YINEAIQBA"; // <GPGSID>
    public const string achievement_poison_hand = "CgkI18ub7YINEAIQBQ"; // <GPGSID>
    public const string achievement_ghost_hand = "CgkI18ub7YINEAIQBg"; // <GPGSID>
#else
    public const string achievement_super_rock = "achievement_super_rock";
    public const string achievement_super_scissors = "achievement_super_scissors";
    public const string achievement_super_paper = "achievement_super_paper";
    public const string achievement_poison_hand = "achievement_poison_hand";
    public const string achievement_ghost_hand = "achievement_ghost_hand";
#endif

    public static void ShowAchievements() {
#if SOCIAL && !UNITY_EDITOR
        if (Social.localUser.authenticated) {
            Social.ShowAchievementsUI();
        }
#endif
    }

    public static bool ReachSuperRock(int kills) {
#if SOCIAL && !UNITY_EDITOR
        if (kills >= superRockKills && Social.localUser.authenticated) {
            Social.ReportProgress(achievement_super_rock, 100, (success) => {
                Debug.Log("Reach 'Super Rock' achievement " + (success ? "succeeded" : "failed"));
            });
            return true;
        }
#endif
        return false;
    }

    public static bool ReachSuperScissors(int kills) {
#if SOCIAL && !UNITY_EDITOR
        if (kills >= superScissorsKills && Social.localUser.authenticated) {
            Social.ReportProgress(achievement_super_scissors, 100, (success) => {
                Debug.Log("Reach 'Super Scissors' achievement " + (success ? "succeeded" : "failed"));
            });
            return true;
        }
#endif
        return false;
    }

    public static bool ReachSuperPaper(int kills) {
#if SOCIAL && !UNITY_EDITOR
        if (kills >= superPaperKills && Social.localUser.authenticated) {
            Social.ReportProgress(achievement_super_paper, 100, (success) => {
                Debug.Log("Reach 'Super Paper' achievement " + (success ? "succeeded" : "failed"));
            });
            return true;
        }
#endif
        return false;
    }

    public static bool ReachPoisonHand(int kills) {
#if SOCIAL && !UNITY_EDITOR
        if (kills >= poisonHandKills && Social.localUser.authenticated) {
            Social.ReportProgress(achievement_poison_hand, 100, (success) => {
                Debug.Log("Reach 'Poison Hand' achievement " + (success ? "succeeded" : "failed"));
            });
            return true;
        }
#endif
        return false;
    }

    public static bool ReachGhostHand(int kills) {
#if SOCIAL && !UNITY_EDITOR
        if (kills >= ghostHandKills && Social.localUser.authenticated) {
            Social.ReportProgress(achievement_ghost_hand, 100, (success) => {
                Debug.Log("Reach 'Ghost Hand' achievement " + (success ? "succeeded" : "failed"));
            });
            return true;
        }
#endif
        return false;
    }

    #endregion

    #region Leaderboard

#if UNITY_ANDROID && !NO_GPGS
    public const string leaderboard_score = "CgkI18ubsdfy17HGDB"; // GPGSid.leaderboard
#else
    public const string leaderboard_score = "FastPass";
#endif

    public static void ShowLeaderboards() {
#if SOCIAL && !UNITY_EDITOR
        if (Social.localUser.authenticated) {
            Social.ShowLeaderboardUI();
        }
#endif
    }

    public static void ReportScore(int score) {
#if SOCIAL && !UNITY_EDITOR
        if (Social.localUser.authenticated) {
            Social.ReportScore(score, leaderboard_score, (success) => {
                Debug.Log("Report score " + (success ? "succeeded" : "failed"));
            });
        }
#endif
    }

    #endregion

}
