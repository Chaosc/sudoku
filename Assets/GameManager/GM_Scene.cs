﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public partial class GameManager : MonoBehaviour {

    public string currentScene { get { return SceneManager.GetActiveScene().name; } }
    public string targetScene { get; set; }
    private bool loadingScene = false;

    public void LoadTargetScene() {
        LoadScene(targetScene);
    }

    public void SwitchScene(string name, bool useLoadingScreen = false) {
        if (currentScene != name) {
            LoadScene(name);
        }
    }

    public void LoadScene(string name, bool useLoadingScreen = false) {
        if (!loadingScene) {
            if (useLoadingScreen) {
                StartCoroutine(LoadSceneAsync(name));
            } else {
                SceneManager.LoadScene(name);
                EzAnalytics.LogScreen(name);
            }
        }
    }

    public void ReloadScene(bool useLoadingScreen = false) {
        LoadScene(currentScene, useLoadingScreen);
    }

    IEnumerator LoadSceneAsync(string name) {
        loadingScene = true;
        EzTiming.KillCoroutines();
        Panel loadingScreen = Panel.Open("LoadingScreen");
        ProgressBar progressBar = null;
        if (loadingScreen != null) {
            progressBar = loadingScreen.GetComponentInChildren<ProgressBar>();
        }
        AsyncOperation async = SceneManager.LoadSceneAsync(name);
        async.allowSceneActivation = false;
        while (async.progress < 0.9f) {
            if (progressBar != null) {
                progressBar.progress = async.progress;
            }
            yield return null;
        }
        if (progressBar != null) {
            progressBar.progress = 1;
        }
        yield return new WaitForSeconds(1f);
        EzAnalytics.LogScreen(name);
        loadingScene = false;
        async.allowSceneActivation = true;
    }

    private Dictionary<string, System.Action> sceneActions = new Dictionary<string, System.Action>();

    public void AddSceneAction(string name, System.Action action) {
        if (!sceneActions.ContainsKey(name)) {
            sceneActions.Add(name, action);
        }
    }

    public void OnStartScene() {
        if (sceneActions.ContainsKey(currentScene)) {
            sceneActions[currentScene].Invoke();
        }
    }

}
