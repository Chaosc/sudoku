﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public int testCoins = -1;
    public int initCoins = 1000;

    private const string PLAY_TIMES = "PLAY_TIMES";
    private const string NAME = "NAME";
    private const string COINS = "COINS";
    private const string SCORE = "SCORE";
    private const string BEST_SCORE = "BEST_SCORE";
    private const string UNLOCKED_LEVELS = "UNLOCKED_LEVELS";
    private const string SCORE_OF_LEVEL = "SCORE_OF_LEVEL";
    private const string STAR_OF_LEVEL = "STAR_OF_LEVEL";
    private const string TIME_OF_LEVEL = "TIME_OF_LEVEL";
    private const string COST_OF_LEVEL = "COST_OF_LEVEL";
    private const string VIBRATE = "VIBRATE";
    private const string NO_ADS = "NO_ADS";
    private const string SAVA_LEVEL = "SAVA_LEVEL";
    private const string MASTER_NUM = "MASTER_NUM";
    private const string CHALLENGE_NUM = "CHALLENGE_NUM";
    private const string HINT_NUM = "HINT_NUM";

    private const string DIF1_NUM = "DIF1_NUM";
    private const string DIF2_NUM = "DIF2_NUM";
    private const string DIF3_NUM = "DIF3_NUM";
    private const string DIF4_NUM = "DIF4_NUM";
    private const string DIF5_NUM = "DIF5_NUM";

    private const string TIME_BTN = "TIME_BTN";
    private const string HINT_BTN = "HINT_BTN";
    private const string CHAN_BTN = "CHAN_BTN";
    private const string PENCUL_HINT = "PENCUL_HINT";
    private const string RM_HINT = "RM_HINT";

    private const string UNLOCKMASTER = "UNLOCKMASTER";


    public bool noAds {
        get { return EzPrefs.GetBool(NO_ADS); }
        set { EzPrefs.SetBool(NO_ADS, value); }
    }
    public int playTimes;

    private const string GUIDED = "GUIDED";
    public bool guided {
        get { return EzPrefs.GetBool(GUIDED); }
        set { EzPrefs.SetBool(GUIDED, value); }
    }

    public string playerName;
    public Sprite playerAvatar;
    public int coins;
    public int score;
    public int bestScore = 9999999;
    public int unlockedLevels;

    public int masterNum;
    public int challengeNum;
    public int HintNum;
    public int dif1Num;
    public int dif2Num;
    public int dif3Num;
    public int dif4Num;
    public int dif5Num;

    public bool isChallenge = false;

    public int SubPanelCount = 0;

    

    void LoadPlayerData() {
        playTimes = EzPrefs.GetInt(PLAY_TIMES, 0);
        EzPrefs.SetInt(PLAY_TIMES, ++playTimes);
        playerName = EzPrefs.GetString(NAME, playerName);
        coins = testCoins >= 0 ? testCoins : EzPrefs.GetInt(COINS, initCoins);
        score = EzPrefs.GetInt(SCORE, 0);
        bestScore = EzPrefs.GetInt(BEST_SCORE, 9999999);
        unlockedLevels = testLevels > 0 ? testLevels : EzPrefs.GetInt(UNLOCKED_LEVELS, 1);
        masterNum = EzPrefs.GetInt(MASTER_NUM,0);
        challengeNum = EzPrefs.GetInt(CHALLENGE_NUM, 0);
        HintNum = EzPrefs.GetInt(HINT_NUM, 0);
        dif1Num = EzPrefs.GetInt(DIF1_NUM, 0);
        dif2Num = EzPrefs.GetInt(DIF2_NUM, 0);
        dif3Num = EzPrefs.GetInt(DIF3_NUM, 0);
        dif4Num = EzPrefs.GetInt(DIF4_NUM, 0);
        dif5Num = EzPrefs.GetInt(DIF5_NUM, 0);
    }

    public void FixupPlayerName() {
        if (string.IsNullOrEmpty(playerName)) {
            UpdatePlayerName(Localization.GetText("Guest") + Random.Range(123456789, 987654321));
        }
    }

    public bool IsShowPenculHint() {
        return EzPrefs.GetBool(PENCUL_HINT, false);
    }

    public void ShowPenculHint() {
        EzPrefs.SetBool(PENCUL_HINT, true);
    }

    public bool IsShowRmHint() {
        return EzPrefs.GetBool(RM_HINT, false);
    }

    public void ShowRmHint() {
        EzPrefs.SetBool(RM_HINT, true);
    }

    public bool IsUnlockMaster() {
        return EzPrefs.GetBool(UNLOCKMASTER, false);
    }

    public void UnlockMaster() {
        EzPrefs.SetBool(UNLOCKMASTER, true);
    }

    public void CancelUnlockMaster() {
        EzPrefs.SetBool(UNLOCKMASTER, false);
    }

    public bool GetTimeBtn() {
        return EzPrefs.GetBool(TIME_BTN, true);
    }   

    public void ChangeTimeBtn() {
        EzPrefs.SetBool(TIME_BTN, !GetTimeBtn());
    }

    public bool GetHintBtn() {
        return EzPrefs.GetBool(HINT_BTN, true);
    }

    public void ChangeHintBtn() {
        EzPrefs.SetBool(HINT_BTN, !GetHintBtn());
    }

    public bool GetChanBtn() {
        return EzPrefs.GetBool(CHAN_BTN, false);
    }
    public void ChangeChanBtn() {
        EzPrefs.SetBool(CHAN_BTN, !GetChanBtn());
    }


    public string CheckSavaLevel() {
        return EzPrefs.GetString("LEVEL","");
    }

    public string CheckSavaLittleLevel() {
        return EzPrefs.GetString("LITTLELEVEL", "");
    }

    public void CostMasterNum() {
        EzPrefs.SetInt(MASTER_NUM, --masterNum);
    }
    public void GainMasterNum() {
        EzPrefs.SetInt(MASTER_NUM, ++masterNum);
    }
    
    public void AddChanlengeNum() {
        EzPrefs.SetInt(CHALLENGE_NUM, ++challengeNum);
    }

    public void CostHintNum() {
        EzPrefs.SetInt(HINT_NUM, --HintNum);
    }

    public void GainHintNum(int num) {
        HintNum += num;
        EzPrefs.SetInt(HINT_NUM, HintNum);
    }

    public void GainDif1Num() {
        EzPrefs.SetInt(DIF1_NUM, ++dif1Num);
    }
    public void GainDif2Num() {
        EzPrefs.SetInt(DIF2_NUM, ++dif2Num);
    }
    public void GainDif3Num() {
        EzPrefs.SetInt(DIF3_NUM, ++dif3Num);
    }
    public void GainDif4Num() {
        EzPrefs.SetInt(DIF4_NUM, ++dif4Num);
    }
    public void GainDif5Num() {
        EzPrefs.SetInt(DIF5_NUM, ++dif5Num);
    }


    public void SetSavaLevel(string Level) {
        EzPrefs.SetString("LEVEL", Level);
    }

    public void SetSavaLittleLevel(string Level) {
        EzPrefs.SetString("LITTLELEVEL", Level);
    }

    public bool CheckLevelIsClear(string LevelName) {
       return EzPrefs.GetBool(LevelName,false);
    }

    public void LevelClear(string LevelName) {
        EzPrefs.SetBool(LevelName, true);
    }
    public void LevelClearFalse(string LevelName) {
        EzPrefs.SetBool(LevelName, false);
    }

    public void UpdatePlayerName(string name) {
        playerName = name;
        EzPrefs.SetString(NAME, playerName);
    }

    public void GainCoins(int gains, string from = "") {
        coins += gains;
        EzPrefs.SetInt(COINS, coins);
        EzAnalytics.LogEvent("Coin", "Gain", from, gains);
    }

    public int CostCoins(int costs, string to = "") {
        if (costs > coins) return -1;
        coins -= costs;
        EzPrefs.SetInt(COINS, coins);
        EzAnalytics.LogEvent("Coin", "Cost", to, costs);
        return coins;
    }

    public void GainScore(int gains) {
        UpdateScore(score + gains);
    }

    public bool UpdateScore(int score) {
        this.score = score;
        EzPrefs.SetInt(SCORE, score);
        if (score < bestScore) {
            bestScore = score;
            EzPrefs.SetInt(BEST_SCORE, bestScore);
            ReportScore(score);
            return true;
        }
        return false;
    }

    public bool UnlockLevel() {
        if (unlockedLevels < maxLevels) {
            ++unlockedLevels;
            EzPrefs.SetInt(UNLOCKED_LEVELS, unlockedLevels);
            return true;
        }
        return false;
    }

    public int GetScoreOfCurrentLevel() {
        return GetScoreOfLevel(currentLevel);
    }

    public void UpdateScoreOfCurrentLevel(int score) {
        UpdateScoreOfLevel(currentLevel, score);
    }

    public int GetScoreOfLevel(int levelId) {
        return EzPrefs.GetInt(SCORE_OF_LEVEL + "_" + levelId);
    }

    public void UpdateScoreOfLevel(int levelId, int score) {
        string key = SCORE_OF_LEVEL + "_" + levelId;
        if (score > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, score);
        }
    }

    public int GetStarOfCurrentLevel() {
        return GetStarOfLevel(currentLevel);
    }

    public void UpdateStarOfCurrentLevel(int star) {
        UpdateStarOfLevel(currentLevel, star);
    }

    public int GetStarOfLevel(int levelId) {
        return EzPrefs.GetInt(STAR_OF_LEVEL + "_" + levelId);
    }

    public void UpdateStarOfLevel(int levelId, int star) {
        string key = STAR_OF_LEVEL + "_" + levelId;
        if (star > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, star);
        }
    }

    public float GetTimeOfCurrentLevel() {
        return GetTimeOfLevel(currentLevel);
    }

    public void UpdateTimeOfCurrentLevel(float time) {
        UpdateTimeOfLevel(currentLevel, time);
    }

    public float GetTimeOfLevel(int levelId) {
        return EzPrefs.GetFloat(TIME_OF_LEVEL + "_" + levelId, float.PositiveInfinity);
    }

    public void UpdateTimeOfLevel(int levelId, float time) {
        string key = TIME_OF_LEVEL + "_" + levelId;
        if (time < EzPrefs.GetFloat(key)) {
            EzPrefs.SetFloat(key, time);
        }
    }

    public int GetCostOfCurrentLevel() {
        return GetCostOfLevel(currentLevel);
    }

    public void UpdateCostOfCurrentLevel(int cost) {
        UpdateCostOfLevel(currentLevel, cost);
    }

    public int GetCostOfLevel(int levelId) {
        return EzPrefs.GetInt(COST_OF_LEVEL + "_" + levelId, int.MaxValue);
    }

    public void UpdateCostOfLevel(int levelId, float time) {
        string key = COST_OF_LEVEL + "_" + levelId;
        if (time < EzPrefs.GetFloat(key)) {
            EzPrefs.SetFloat(key, time);
        }
    }

    public bool vibrateEnabled {
        get { return EzPrefs.GetInt(VIBRATE, 1) != 0; }
        set { EzPrefs.SetInt(VIBRATE, value ? 1 : 0); }
    }

    public void Vibrate() {
#if UNITY_ANDROID || UNITY_IOS
        if (vibrateEnabled) Handheld.Vibrate();
#endif
    }

}
